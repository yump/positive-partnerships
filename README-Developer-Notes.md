Positive Partnerships
======================

Gulp / Node / NPM / NVM
----------------------------------
- Use NVM if you run into issues with Gulp
- Use Node 8.11.3 (or probably earlier versions will work too)
= Currently, Gulp will fail with 10.16.3 (or I guess later versions)

Yump Craft Boilerplate
======================

Developed by Yump - 2016  
www.yump.com.au

This repository should house the latest version of Craft (2.x.x), along with Yump's preferred customisations.

Important notes (Stick to top)
------------------------------
* Please DO NOT update SimpleMap plugin because we found out 'ownerLocale' and 'parts' fields are not saved once updated.

Why make our own version of Craft?
----------------------------------

Although Craft is great, there are several things that we need to add to every project:

* Our own (slighly modified) folder structure (ensuring requests to `/craft/` folder are blocked)
* Yump's ultimate `.htaccess` file
* `robots.txt` and `robots_staging.txt`
* Initial phploy config files
* Separate dev/staging/production profiles, all set to useful defaults
* Pre-built layouts and template files (using refined and well-tested best-practices)
* Large list of standard matrix block templates
* Useful Craft snippets (breadcrumbs, spinners, etc.)
* Full of useful plugins that are likely to be used on 80%+ of websites
   - Neo (allows nested Matrix fields)
   - FieldNotes
   - LinkIt field
   - Quickfield
   - Relabel
   - SEO
   - Supertable
   - Tablemaker (for clients to create their own custom tables)
   - VideoEmbed
   - Visor (allows easy access to "Edit this page" from frontend)
* Other useful libraries already embedded (`/vendor/` folder)
   - Bootstrap
   - jQuery
   - Lity
* Templates for creating CMS documentation
* Yump's database backup script
* Yump logos (all common variations)
* Craft already populated with several useful fields (Google Analytics, GTM, custom scripts etc.)
* Asset storage folders and profiles already setup
* Designed for easy duplication on staging (copy, paste, make new database, done!)

And possibly in future:

* A pre-built collection of matrix blocks
* Automated tests for common issues
* NodeJS/gulp stuff
* Generic documentation to kickstart creating docs for clients


General Notes 
-------------

* To check email error log, go to `craft/storage/runtime`, and find the log file named under the plugin (which should be the client plugin folder) in this case. This is not working yet, need to find a way to make this work.

## Layout

  - Use this for content wrapped with Bootstrap `.container`:

        {% extends "_layout" %}
        {% block content %}
           ...
        {% endblock %}

  - Use this for content wrapped with full-width `<main>` tag:

  		{% extends "_layout" %}
  		{% block main %}
   		   ...
  		{% endblock %}

## Global fields

   - In CMS, to create a global field, we need to set up that field first, and then add that in settings -> Globals. Then, admin can set / update the value under 'Globals'
   - to retrieve the value of a global field:
      - In PHP, there's a function set up in YumpNova_GeneralService, so call `craft()->yumpNova_general->getGlobalFieldValue($globalSetHandle, $fieldHandle)`.
      - In Twig, use `{{ globalSetHandle.fieldHandle }}`

## Our plugins:

  - YumpNova: general useful functions for Craft
  - YumpForm: form for Craft

## Include CSS / JS:

  - `{% includeCss %} {% includeJs %} {% includeCssFile %} {% includeJsFile %}`
  - For CSS, they will be put before the closing of <head> tag, or wherever we put the code: `{{ getHeadHtml() }}`
  - For JS, they will be put before the closing of <body>, or wherever we put the code: `{{ getFootHtml() }}`
  - If we want to move a CSS / JS to the top, use `first` parameter
  - To use include js script:

        {% set chosenScript %}
          // ... js codes (don't need the <script> tag)
        {% endset %}
        {% includeJs chosenScript %}

  - Reference: https://craftcms.com/docs/templating/includecssfile

## To use functions in our own plugins:

  - On front-end template (Twig): call `craft.pluginHandle.functionName()` (e.g. craft.yumpNova.ieVersion()), this normall call the function in YumpNovaVariable (for example) class, but we used a __call() function in that class to redirect all undefined functions to YumpNova_GeneralService class. In this way, we don't have to create wrapper functions in the YumpNovaVariable class for all the YumpNova_GeneralService functions. (see https://craftcms.com/docs/plugins/variables)
  - In PHP, call craft()->serviceHandle->functionName() (e.g. craft()->yumpNova_general->ieVersion()).
  - It doesn't seem to have a way to call PluginVariable class variables / functions in PHP.
  - So, basicly, we will put everything in plugin service class, for both PHP and Twig to access them. Unless there are some variables / functions we want only Twig to access them, then we can put in plugin variable class.

## Mobile_Detect functions

Call craft()->yumpNova_general->mobileDetectFunction() in PHP, or craft.yumpNova.mobileDetectFunction() in Twig. We make it possible by using __call method in YumpNova_GeneralService class, to attempt undefined functions in Mobile_Detect class.

## Bootstrap Styling - How to Modify

src/styles/base/bs-variables has our variables that override the defaults set by Bootstrap. The full list of variables you can adjust from our own Sass files are listed in node_modules\bootstrap\scss\variables.scss. 

## Templates and frontend

The menu uses the smartmenu bootstrap library, loaded by Gulp. The path to the styling is included in the gulpfile and imported in _styles.scss