Positive Partnerships Maintenance Log
=====================================

# Important Notes
* Please DO NOT update SimpleMap plugin because we found out 'ownerLocale' and 'parts' fields are not saved once updated.

Latest maintenance entry to the top. 

#### Important notes for next maintenance

### 15 Feb 2022
* Tested all backend and templates - all good
* Check blocks found some content block's help doc outdated - all good now 
  help doc updated: 
  - reusable snippet
  - icon with text
  - accordion
* No errors in logs and console
* Database auto backup - all good
* cPanel Disk usage : (53%) - removed some historical database backup files all good
* Broken links report:
  - 404 Not found
    https://aca.edu.au/terms-of-use/
    Linked from: https://www.positivepartnerships.com.au/terms-of-use
  - Timeout
    https://www.icannetwork.com.au/
    Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-transitions-in-the-secondary-years-and-beyond
                 https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-simplified/transitions-simplified-chinese
                 https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-traditional/transitions-traditional-chinese
  - 404 Not found
    https://www.positivepartnerships.com.au/workshops-online-learning/online-learning
    Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/sensory-resources

### 29 November 2021
* Tested all backend and templates - all good
* No errors in logs and console
* Database auto backup all good
* cPanel Disk usage : (52.41%) all good
* Broken links report:

404 Not found	
https://www.positivepartnerships.com.au/uploads/positive_partnership_rap_2019_11.03.19_web.pdf

Linked from: https://www.positivepartnerships.com.au/acknowledgement-of-country
	

404 Not found	
https://aspect.zoom.us/meeting/register/tJ0vdeqvpzgtHdJ4AMVDYslT4EBWmk0k59lR

Linked from: https://www.positivepartnerships.com.au/news/2021/yarning-and-sharing-about-autism
	

404 Not found	
https://aspect.zoom.us/meeting/register/tJ0rc--gpzkuGtcXa3rfALd_GFo5EqUXIGvk

Linked from: https://www.positivepartnerships.com.au/news/2021/yarning-and-sharing-about-autism
	

404 Not found	
https://aspect.zoom.us/meeting/register/tJ0pf-qtrT4uE9O8449i4gsgecpCJ2Cz2FQc

Linked from: https://www.positivepartnerships.com.au/news/2021/yarning-and-sharing-about-autism
	

404 Not found	
https://aspect.zoom.us/meeting/register/tJwod-msrToqHtGX8a_5xXy6ojWH2rhSqowi

Linked from: https://www.positivepartnerships.com.au/news/2021/yarning-and-sharing-about-autism
	

404 Not found	
https://www.positivepartnerships.com.au/uploads/Discrepancies-between-dimensions-of-interoception-in-Autism-Implications-for-emotion-and-anxiety.pdf

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/interoception
	

404 Not found	
https://www.positivepartnerships.com.au/uploads/Interoception-and-emotion.pdf

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/interoception
	

404 Not found	
https://www.positivepartnerships.com.au/uploads/Interoception-and-Mental-Health-A-Roadmap.pdf

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/interoception
	

404 Not found	
https://www.education.vic.gov.au/school/teachers/learningneeds/Pages/individualeducationplan.aspx

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources
	

403 Forbidden	
https://publicdocumentcentre.education.tas.gov.au/library/Shared%20Documents/Good-Teaching-Inclusive-Schools-Disability-Focus.pdf

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources
	

404 Not found	
https://www.positivepartnerships.com.au/uploads/Djarmbi-the-different-kookaburra_web2.pdf

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/sensory-resources
	

404 Not found	
https://education.nsw.gov.au/teaching-and-learning/learning-from-home/learning-at-home/learning-resources

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-learning-at-home
	

404 Not found	
https://education.nsw.gov.au/teaching-and-learning/learning-from-home/learning-at-home/learning-resources/learning-resources

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-learning-at-home
	

404 Not found	
https://education.nsw.gov.au/public-schools/going-to-a-public-school/translated-documents/learning-from-home

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-learning-at-home
	

404 Not found	
https://education.nsw.gov.au/teaching-and-learning/learning-from-home/learning-at-home/parent-and-carers-toolkit

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-learning-at-home
	

404 Not found	
https://education.qld.gov.au/schools-educators/distant-education

Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-learning-at-home

### 11 August 2021

* Craft and all plugins are already up to date

Backend:
* Tested all backend sections, assets upload functionality, entry saving functionality and content blocks - all good

Templates and search functionality - all good
* Workshops - Pages - Resource
* Search functionality & search results

* Broken Links report:

Other broken links:

404 Not found	
https://www.education.gov.au/students-disability
Linked from: https://www.positivepartnerships.com.au/about-us/what-we-do

404 Not found	
https://education.gov.au/positive-partnerships
Linked from: https://www.positivepartnerships.com.au/about-us/what-we-do

404 Not found	
https://www.gonoodle.com/blog/gonoodle-games-movement-app-for-kids/
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

404 Not found	
https://allplayfooty.org.au/coaches/asd/
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

404 Not found	
https://docs.education.gov.au/system/files/doc/other/planningforpersonalisedlearningandsupportnationalresource.pdf
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources

403 Forbidden	
https://publicdocumentcentre.education.tas.gov.au/library/Shared%20Documents/Good-Teaching-Inclusive-Schools-Disability-Focus.pdf
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources

404 Not found	
https://www.mq.edu.au/about/about-the-university/faculties-and-departments/faculty-of-human-sciences/departments-and-centres/department-of-educational-studies/news-and-events/news/news/autism-advocacy-and-research-misses-the-mark-if-autistic-people-are-left-out
Linked from: https://www.positivepartnerships.com.au/news/2020/autismmq

### 26 May 2021

* Craft and all plugins are up to date

Tested - all good : 

Backend: 
 * Tested all backend sections
 * Tested assets upload functionality 
 * Tested entry saving functionality 
 * Tested content blocks 

Templates
* Workshops 
* Pages 
* Resource  
* Search functionality & search results 

Reviewed Errors

* Broken Links report:


Result	URL	
Too many redirects	

http://positive-partnerships.staging7.yump.com.au/resources/parents-and-carers
Linked from: 
https://www.positivepartnerships.com.au/resources

403 Forbidden	
https://www.autismcrc.com.au/slideshow/national-autism-guideline-now-available
Linked from: 
https://www.positivepartnerships.com.au/all-about-autism
https://www.positivepartnerships.com.au/news/2016/autism-crc-national-guidelines

404 Not found	
https://www.positivepartnerships.com.au/{entry:677:url}
Linked from: 
https://www.positivepartnerships.com.au/all-about-autism

Host not found	
mailto:positivepartnerships@autimspectrum.org.au
Linked from: 
https://www.positivepartnerships.com.au/terms-of-use


Host not found	
https://ahrc.eq.edu.au/services/fba-tool/help/environmental-audit
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/sensory-resources

404 Not found	
https://www.autism.org.au/coronavirus-covid-19-update/
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

404 Not found	
https://www.acd.org.au/wp-content/uploads/2020/09/I-am-going-back-to-school-story_accessible-2.pdf
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

404 Not found	
https://allplaylearn.org.au/covid-support/education-settings/plan-onsite-learning/primary-handwashing-skills/
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

404 Not found	
https://allplayfooty.org.au/coaches/asd/
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

404 Not found	
https://allplayfooty.org.au/aboriginal-and-torres-strait-islander-footy/
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

404 Not found	
http://www.rda.org.au/default-landing.aspx
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

404 Not found	
http://www.australiandisabilitysport.com.au/sa.html
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

403 Forbidden	
https://publicdocumentcentre.education.tas.gov.au/Documents/Good-Teaching-Inclusive-Schools-Disability-Focus.pdf
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources

Connect error	
https://www.lifeanimateddoc.com/
Linked from: 
https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/interest-based-learning

404 Not found	
https://soundcloud.com/callmenovatrippa
Linked from: 
https://www.positivepartnerships.com.au/news/2016/world-autism-awareness-day

404 Not found	
https://www.autismhelp.info/wp-content/uploads/pdf/Teen-Years-School-and-Post-School-Options.pdf
Linked from: 
https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-simplified/transitions-simplified-chinese
https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-traditional/transitions-traditional-chinese

404 Not found	
https://www.mq.edu.au/about/about-the-university/faculties-and-departments/faculty-of-human-sciences/departments-and-centres/department-of-educational-studies/news-and-events/news/news/autism-advocacy-and-research-misses-the-mark-if-autistic-people-are-le…
Linked from: 
https://www.positivepartnerships.com.au/news/2020/autismmq


### 5 February 2021

* Craft and all plugins are up to date

Tested - all good : 

Backend: 
 * Tested all backend sections
 * Tested assets upload functionality 
 * Tested entry saving functionality 
 * Tested content blocks 

Templates
* Workshops 
* Pages 
* Resource  
* Search functionality & search results 

Reviewed Errors

* Broken Links report:

403 Forbidden	

*  https://www.autismcrc.com.au/slideshow/national-autism-guideline-now-available

Linked from

https://www.positivepartnerships.com.au/all-about-autism
https://www.positivepartnerships.com.au/news/2016/autism-crc-national-guidelines

* 404 Not found	
https://www.positivepartnerships.com.au/{entry:677:url}

Linked from: 

https://www.positivepartnerships.com.au/all-about-autism

* Too many redirects	
http://positive-partnerships.staging7.yump.com.au/resources/parents-and-carersRedirected

Linked from: 

https://www.positivepartnerships.com.au/resources

* Host not found

mailto:positivepartnerships@autimspectrum.org.au
Linked from: https://www.positivepartnerships.com.au/terms-of-use

* 404 Not found	
https://www.autism.org.au/coronavirus-covid-19-update/

Linked from

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/health-hygiene-in-the-home

* 404 Not found	

https://www.acd.org.au/wp-content/uploads/2020/09/I-am-going-back-to-school-story_accessible-2.pdf

Linked from: 

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

* 404 Not found	

https://allplaylearn.org.au/covid-support/education-settings/plan-onsite-learning/primary-handwashing-skills/

Linked from: 

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change

* 404 Not found	

https://au.specialisterne.com/

Linked from: 

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-transitions-in-the-secondary-years-and-beyond

https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-simplified/transitions-simplified-chinese
https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-traditional/transitions-traditional-chinese

* 403 Forbidden	

https://publicdocumentcentre.education.tas.gov.au/Documents/Good-Teaching-Inclusive-Schools-Disability-Focus.pdf

Linked from: 

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/iep-resources

* Connect error	

https://www.lifeanimateddoc.com/

Linked from: 

https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/interest-based-learning

* 404 Not found	

https://www.autismhelp.info/wp-content/uploads/pdf/Teen-Years-School-and-Post-School-Options.pdf

Linked from

https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-simplified/transitions-simplified-chinese

https://www.positivepartnerships.com.au/resources/in-other-languages/chinese-traditional/transitions-traditional-chinese

* 404 Not found	

https://www.mq.edu.au/about/about-the-university/faculties-and-departments/faculty-of-human-sciences/departments-and-centres/department-of-educational-studies/news-and-events/news/news/autism-advocacy-and-research-misses-the-mark-if-autistic-people-are-left-out

Linked from: 

https://www.positivepartnerships.com.au/news/2020/autismmq

Misc - all good
* SEO functionality and display
* Disk usage was  53%
### 11 November 2020

#### By Azadeh

* Craft and all plugins are up to date

Tested - all good : 

Backend: 
 * Tested all backend sections
 * Tested assets upload functionality 
 * Tested entry saving functionality 
 * Tested content blocks 

Templates
* Workshops 
* Pages 
* Resource  
* Search functionality & search results 

Reviewed Errors:

* Broken Links report:

 1.	 http://positive-partnerships.staging7.yump.com.au/resources/parents-and-carers
Linked from: https://www.positivepartnerships.com.au/resources
2.	 mailto:positivepartnerships@autimspectrum.org.au
Linked from: https://www.positivepartnerships.com.au/terms-of-use
3.  https://www.autism.org.au/coronavirus-covid-19-update/
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change and 1 more
4. 	https://www.acd.org.au/wp-content/uploads/2020/09/I-am-going-back-to-school-story_accessible-2.pdf
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change
5. 	https://allplaylearn.org.au/covid-support/education-settings/plan-onsite-learning/primary-handwashing-skills/
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/returning-to-school-preparing-for-change
6.  https://www.autismhelp.info/wp-content/uploads/pdf/Teen-Years-School-and-Post-School-Options.pdf
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/supporting-transitions-in-the-secondary-years-and-beyond
7.  http://www.fpt.asn.au/
Linked from: https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/talking-about-sexuality
8.  https://www.mq.edu.au/about/about-the-university/faculties-and-departments/faculty-of-human-sciences/departments-and-centres/department-of-educational-studies/news-and-events/news/news/autism-advocacy-and-research-misses-the-mark-if-autistic-people-are-left-out
Linked from: https://www.positivepartnerships.com.au/news/2020/autismmq

Misc - all good
* SEO functionality and display
* Disk usage was almost full - reduced it to 53% 


### 4 August 2020
####  By Azadeh
* Backed up Production DB Offsite and local
* Checked plugins - no updates
* Checked that backups are running on cPanel - all good
* Checked disk usage - It was 110% over full, so as we had newest backups already, we deleted the very old ones until June 1st 2020, now it is 58% , all good.
* Reviewing server error logs for any issues of concern
* Checked retour statistics, all good.
* Checked broken links and found these links broken:
https://positivepartnerships.arlo.co/register?sgid=3981031230234a1cbe509b368e36e23e. (This link is invalid or refers to an event that is no longer available. the link is in this page:=>. https://www.positivepartnerships.com.au/workshops-online-learning/webinars/wellbeing-and-resilience)
https://raisingchildren.net.au/autism/behaviour/understanding-bhaviour/sensory-sensitivities-asd (the link is in this page => https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/sensory-resources)
https://noisli.com/ (the link is in this page => https://www.positivepartnerships.com.au/resources/practical-tools-information-sheets/technology-and-online-learning)

### 5 May 2020
#### By Dean Wilson (dean@yump.com.au)
* Backed up Production DB
* Updated Craft to 2.9.2
* Checekd that backups are running on cPanel - acronis - all good
* Checked disk usage and other usage stats - no problem
* Deployed to staging and tested site - all good 
* Checked retour statistics - there are many URL's that are being hit as 404's that should be redirected. Ignore the first couple and focus on the pages from "/planning-matirx" 
	* 404 for /global/img/icon-share.png - cannot find this in the code anywhere
* Checked error log on cPanel - just 404's for favicon paths - removed unused file with those paths in it
* Enabled staging moved page on staging and fixed broken image
* Deployed updates to Production - tested site - all good

### 27 February 2020
#### By Azadeh
* Backed up Production DB
* Imported Production DB into local site
* Downloaded all New Uploads to local site as a backup 
* Updated Craft to 2.9.1
* Checked plugins - no update
* Checekd that backups are running on cPanel - acronis - all good
* Checked disk usage and other usage stats - It was 98% full, so as we had newest backups already, we deleted the old ones before 18th of February 2020 in the file manager -> public_html/craft/storage/backups  and now it is just 9.8% disk usage which is great.
* Ran Xenu link checking and we found these pages with 404 error. Please check the corresponding pages in the CMS to fix the broken links
	https://www.autismspectrum.org.au/sites/default/files/For%20External%20Distribution%20-%20P-18.01%20Privacy%20of%20Information%20and%20Data.pdf
	error code: 404 (not found), linked from page(s):
	https://www.positivepartnerships.com.au/privacy-policy

	https://www.autismspectrum.org.au/sites/default/files/Privacy-of-Personal-Information-and-Data-Easy-English-2018%20Final.pdf
	error code: 404 (not found), linked from page(s):
	https://www.positivepartnerships.com.au/privacy-policy

	https://www.positivepartnerships.com.au/%7Bentry:677:url%7D
	error code: 404 (not found), linked from page(s):
	https://www.positivepartnerships.com.au/all-about-autism

* Redirects:  There are around 150 unhandeled links (404 errors) which are over 20 hits. Please log in to the CMS and go to Redirects/Statistics to add redirection rules to these unhandled links.
* Pdfs on the site are not displaying in chrome on desktop (which works on mobile and other browsers). We have done a quick research and it seems to be a Chrome issue which is out of our control. If you want us to look into this further, it will be about 2 hour extra time. Please let us know. 

### 25 October 2019
* Backed up Production DB
* Imported Production DB into local site
* Downloaded all New Uploads to local site as a backup 
* Updated Craft to 2.7.10
* Checked plugins - no update
* Checekd that backups are running on cPanel - acronis - all good
* Checked disk usage and other usage stats - no problem
* Deployed to staging and tested site - all good 
* Deployed to production - tested site - all good
* Checked console for any errors - No error
* Checked backend - all good
* Ran Xenu link checking
	* Broken link on Privacy Policy page (https://www.positivepartnerships.com.au/privacy-policy), The link back to aspect site pdf
	* Some broken link in css file - fixed
	
### 22 July 2019
* Backed up Production DB
* Imported Production DB into local site
* Downloaded all New Uploads to local site as a backup 
* Updated Craft Manually to latest version locally
* Checked plugins - none to update except simple maps which has already been identified as problematic to update
* Checekd that backups are running on cPanel - acronis - all good
* Checked disk usage and other usage stats - no problem
* Deployed to staging and tested site - all good 
* Deployed to production - tested site - all good
* Checked console for any errors - missing icons 
	- fixed - removed the links 
* Checked backend - stalled tasks - used option to retry
* Ran Xenu link checking
	* Created report to send to client - a few 404s linked in content of pages
	https://docs.google.com/document/d/1Szh6reG-3DvtR5iWuVWWtc9Lv7Pdu1SQ7mjLUs2ejus/edit?usp=sharing

	* Existing issues:
	  - By changing the language from the top dropdown, the workshop buttons in home page look different thann English.
