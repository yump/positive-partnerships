<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

    // PRODUCTION (and general defaults)
    '*' => [
        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => 'localhost',

        // The prefix to use when naming tables. This can be no more than 5 characters.
        'tablePrefix' => 'craft',

        // The name of the database to select.
        'database' => '',

        // The database username to connect with.
        'user' => '',

        // The database password to connect with.
        'password' => '',
    ],

    // STAGING
    '.staging' => [
        // For easy creation of new sites (while also preventing database contamination)
        // we pick a database based on the hostname, eg. DATABASE.staging.yump.com.au
        // But feel free to change this to whatever suits best
        'database' => 'staging7_positive_partnerships',
        'user' => 'staging7_craft',
        'password' => 'ttRh5VzJ7LM1F4nVwX',
    ],

    // LOCAL DEVELOPMENT
    '.test' => [
        'database' => 'positive_partnerships',
        'user' => 'root',
        'password' => 'root',
    ],

//    'positivepartnerships.com.au' => [
//        'database' => 'positivepartners_craft',
//        'user' => 'positivepartners_admin',
//        'password' => 'eo;#)*]YBgwj',
//    ],

    'pp.test.yump.com.au' => [
        'database' => 'positivepartners_craft',
        'user' => 'positivepartners_admin',
        'password' => 'eo;#)*]YBgwj',
    ],
);
