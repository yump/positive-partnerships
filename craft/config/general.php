<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 *
 * Yump Notes: I've also listed a couple of might-be-useful settings (commented out, with default values assigned.) - Wei
 *
 * There are some settings which could be overridden by .htaccess I guess
 */

date_default_timezone_set ('Australia/Melbourne');
return array(
	// PRODUCTION / default settings
	'*' => array(
		'devMode' => false,
		'omitScriptNameInUrls' => true,
		'allowAutoUpdates' => false,
		
		// if not provided here, Craft will use the one set in CMS
		'siteUrl' => sprintf(
		    "%s://%s",
		    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		    $_SERVER['SERVER_NAME']
		),
		'autoLoginAfterAccountActivation' => true,
		'useEmailAsUsername' => true,
		'overridePhpSessionLocation' => true,
		// 'backupDbOnUpdate' => true,
		// 'restoreDbOnUpdateFailure' => true,
		// 'isSystemOn' => null,
		// 'cacheDuration' => 'P1D',
		// 'phpMaxMemoryLimit' => '256M',
		// 'useCompressedJs' => false,
		
		// 'loginPath' => 'login',
		// 'logoutPath' => 'logout',
		// 'maxInvalidLogins' => 5,
		// 'postLoginRedirect' => '', //default: homepage
		// 'userSessionDuration' => 'PT1H', //1 hour
		// 'rememberedUserSessionDuration' => 'P2W', // 2 weeks
		// 'setPasswordPath' => 'setpassword',
		// 'setPasswordSuccessPath' => '', //default: homepage
		'maxUploadFileSize' => 500*1024*1024, // integer, default: 16MB
		
		// Automin Plugin Configuration
		// 'autominCachingEnabled' => false,
		// 'autominCachePath' => 'minified',
		// 'autominCacheURL' => '/minified',
		// custom variable to determine the files containing all the fundamental sass files (e.g. variables). Can be a string or array. These files are always included and compiled every time we use the yumpsass filter
		// 'preImportSassFiles' => [
		// 	'/src/scss/variables/_settings.scss',
		// 	'/src/scss/variables/_colours.scss',
		// 	'/src/scss/variables/_fonts.scss',
		// 	'/src/scss/variables/_typography.scss',
		// ],

		// See /craft/plugins/yump/controllers/YumpController.php for usage.
		'controllerActionsPassword' => 'LmisCYz6R01lpxyhvP',
		'controllerActionsAllowedIps' => array(
			'127.0.0.1', 
            '203.191.201.182',        // Epic office
            '202.91.44.62',           // depo8
            '43.241.54.230',          // staging.yump.com.au (new server)
            '103.27.34.42',           // PP live server
		),
		'dbBackupFilesLivespan' => 30,

		'websiteGoogleMapApiKey' => '',

		'enableFacebookCommentsOnDocs' => true,    // turn on/off facebook comments on docs section
		'enableLoginWallOnDocs' => true,           // turn on/off login wall on docs section

		'eventbriteOauthToken' => '',
		'eventbriteUserId' => '',

		'yumpCurlCacheFolder' => $_SERVER['DOCUMENT_ROOT'] . '/yump/curl/cache',

		'shareThisPropertyId' => '5bd66437ee8c700011929f8f',

		'workshopsPageId' => 651,
		'newsRootPageId' => 656,
		'supportedVideoUrls' => array(
			'vimeo.com',
			'youtu.be',
			'youtube.com'
		),
        'enableCsrfProtection' => true,
	),

	// local dev
	'.test' => array(
		'devMode' => true,
		'testToEmailAddress' => [
			'devs@yump.com.au',
		],
		'requireUserAgentAndIpForSession' => false,
		'requireMatchingUserAgentForSession' => false,
		// 'serverErrorEmailTo' => [
		// 	'wei@yump.com.au',
		// 	'simon@yump.com.au',
		// ],
	),

	// staging
	'staging' => array(
		// 'devMode' => true,
		'requireUserAgentAndIpForSession' => false,
		'requireMatchingUserAgentForSession' => false,
	),	

	// Production
	'positivepartnerships.com.au' => array(
		'enableFacebookCommentsOnDocs' => false,

		'shareThisPropertyId' => '5bd66414ee8c700011929f8e',
		'isLiveSite' => true,
	),
);
