<?php
/** See https://github.com/aelvan/Imager-Craft#configuration or craft/plugins/imager/config.php for full list */

return array(
	'suppressExceptions' => true, // we don't want image compile errors to stop the application, so we ignore it, and it will return null if error occurs
	'allowUpscale' => false, // don't allow imager to up scale images, so they won't create an image which is larger than original one in size.
);