<?php
namespace Craft;

class BusinessLogicPlugin extends BasePlugin
{

	public function getName()
	{
		return 'Yump Client Plugin Starter Kit';
	}

	public function getDescription()
	{
		return 'Yump quick starter kit for a client plugin development, based on the 3rd party Business Logic Plugin. Refer to "README Devs.md" for instructions.';
	}

	public function getDocumentationUrl()
	{
		return '';
	}

	public function getVersion()
	{
		return '1.0.0';
	}

	public function getSchemaVersion()
	{
		return '0.0.0';
	}

	public function getDeveloper()
	{
		return 'Yump';
	}

	public function getDeveloperUrl()
	{
		return 'https://www.yump.com.au';
	}

	/** 
	 * Plugin init function. Put stuff you want to do before other things in plugin
	 * 
	 * E.g. Some Events you will probably wanna catch & handle here. Craft Plugin Events list: https://craftcms.com/docs/plugins/events-reference
	 */
	public function init() {
        parent::init();
        
        /** 
         * Install PHP libraries via composer into the vendor folder of this plugin, and then run the following line if you want to load them in advance.
         */
        // require CRAFT_PLUGINS_PATH . "businesslogic/vendor/autoload.php";
        
        /**
         * Example event handler
         */
        craft()->entries->onBeforeSaveEntry = function(Event $event) {
            $entry = $event->params['entry'];
            if($event->params['isNewEntry']) {

            	/** get current logged-in user if needed */
            	$currentUser = craft()->userSession->getUser();
                
                /** We can stop the entry from saving by calling the following code if it meets certain condition: */
                // if($conditionIsMet) {
                // 	$event->performAction = false;       
                // }
                     
                /** Post messages for displaying flash messages */
                // $_POST['messageType'] = 'danger';
                // $_POST['postMessage'] = 'Oops, we don\'t allow you from saving this entry because you don\'t like Dainty Sichuan!';
            }
        };

    } // end of init()

    /**
     * Register custom email messages (register their handles, the actual messages can be defined in this plugin folder /translations/en_us.php, or w/e the locale file is.)
     */
    public function registerEmailMessages()
    {
        return array(
            'sent_to_admin_at_the_end_of_world',
            // 'sent_to_user_after_successful_donation',
            // ...
        );
    }

    /**
     * Register custom permission, then these permissions will be available in CMS under settings of user group / users.
     * We can use the craft()->yumpNova_general->checkCurrentUserPermission('permission1 permission2') to check if a user has certain permission.
     */
    // public function registerUserPermissions()
    // {
    //     return array(
    //         'permission1' => array('label' => Craft::t('Grant Permission 1')),
    //         'permission2' => array('label' => Craft::t('Grant Permission 2')),
    //     );
    // }

    // function onAfterInstall() {

    // }

    // function onBeforeInstall() {

    // }

    // function onBeforeUninstall() {
        
    // }
}
