This plugin is based on the plugin 'Business Logic plugin template for Craft CMS', by Lindsey DiLoreto.

Basically this is a starter kit of a Craft plugin, so you don't have to create all these files every time creating a new plugin.

However, we still need to change the name of the plugin in:

1. plugin folder name
2. BusinessLogicPlugin.php file name
3. plugin controller file name
4. plugin service file name
5. plugin variable file name
6. the callee function name in __call() function inside the BusinessLogicVariable.php

Replace 'Business Logic' with your plugin name.

Also, update the settings in BusinessLogicPlugin.php