<?php

/**
 * This file is to define the default email messages in en_us language.
 * If relevant subject line / message body is modified in CMS, it will generate a record in database (craft_emailmessages table), and it will override the settings here.
 *
 * We can use Twig syntax here, e.g. {{ user.firstName }}. To pass a variable into here, we can call craft()->email->sendEmailByKey($user, $key, $variables).
 * $user: the recipient. The 'user' Twig variable will be automatically available in this template.
 * $key: the message template handle, e.g. 'send_to_admin_at_the_end_of_world'. There are some Craft-built-in ones, and we can define our custom ones in plugin.
 * $variables: custom variables
 */

return array (
    'sent_to_admin_at_the_end_of_world_heading' => 'Sent to admin when the end of world',
    'sent_to_admin_at_the_end_of_world_subject' => 'The world is ending!',
    'sent_to_admin_at_the_end_of_world_body' => "Hi {{user.firstName}},\n\n" .
        "The world is ending.\n\n" .
        "<strong style=\"font-size: 16px; color: #000\">Things to do:</strong>\n\n" .
        "Something fun? Maybe Dainty Sichuan?\n\n" .
        "Hope you enjoy the planet's last minutes. See you next world!\n\n" .
        "Team Yump",
);
