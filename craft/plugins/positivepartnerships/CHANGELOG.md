# Positive Partnerships Changelog

## 1.0.0 -- 2018-09-19

* Initial release

Brought to you by [Yump](https://www.yump.com.au)
