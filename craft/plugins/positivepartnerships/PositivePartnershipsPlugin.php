<?php
/**
 * Positive Partnerships plugin for Craft CMS
 *
 * This plugin consists of all Positive Partnerhips Specific Functionality, created by Yump
 *
 * --snip--
 * Craft plugins are very much like little applications in and of themselves. We’ve made it as simple as we can,
 * but the training wheels are off. A little prior knowledge is going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL, as well as some semi-
 * advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://www.yump.com.au
 * @package   PositivePartnerships
 * @since     1.0.0
 */

namespace Craft;

class PositivePartnershipsPlugin extends BasePlugin
{
    /**
     * Called after the plugin class is instantiated; do any one-time initialization here such as hooks and events:
     *
     * craft()->on('entries.saveEntry', function(Event $event) {
     *    // ...
     * });
     *
     * or loading any third party Composer packages via:
     *
     * require_once __DIR__ . '/vendor/autoload.php';
     *
     * @return mixed
     */
    public function init()
    {
        parent::init();

        craft()->on('contactForm.beforeSend', function(ContactFormEvent $event) {
            $message = $event->params['message'];
            $response = $_POST['g-recaptcha-response'];
            $verifyUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=6LfWA6wUAAAAADRO_n1pW3ezZhw142GNCdxbXd5C&response=' . $response;
            $curlResponse = craft()->yump_curl->curl($verifyUrl, array(
                'cacheFolder' => craft()->config->get('yumpCurlCacheFolder'),
                'cacheTimeInMinutes' => 2,
                // 'headers' => [
                    // "Authorization: Bearer " . $token,
                    // "Content-type: application/json",
                // ],
            ));

            $result = $curlResponse['JSON']['success'];

            if ($result == 1) {
                $event->isValid = true;
            } else {
                // Setting $isValid to false will cause a validation error
                // and prevent the email from being sent
                $event->isValid = false;

                $error = $curlResponse['JSON']['error-codes'][0];
                switch ($error) {
                    case 'missing-input-response':
                        $message->addError('message', 'A Google recaptcha response is required. Please try again.');
                        break;
                    case 'timeout-or-duplicate':
                        $message->addError('message', 'The recaptcha response is no longer valid: either it is too old or has been used previously.');
                        break;
                    default:
                        $message->addError('message', 'Google ReCaptcha failure - please contact us on 1300 881 971');
                        break;
                }  
            }
        });

    }

    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('Positive Partnerships');
    }

    /**
     * Plugins can have descriptions of themselves displayed on the Plugins page by adding a getDescription() method
     * on the primary plugin class:
     *
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('This plugin consists of all Positive Partnerhips Specific Functionality, created by Yump');
    }

    /**
     * Plugins can have links to their documentation on the Plugins page by adding a getDocumentationUrl() method on
     * the primary plugin class:
     *
     * @return string
     */
    public function getDocumentationUrl()
    {
        return '???';
    }

    /**
     * Plugins can now take part in Craft’s update notifications, and display release notes on the Updates page, by
     * providing a JSON feed that describes new releases, and adding a getReleaseFeedUrl() method on the primary
     * plugin class.
     *
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a plugin’s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1.0.0';
    }

    /**
     * Returns the developer’s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Yump';
    }

    /**
     * Returns the developer’s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'https://www.yump.com.au';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     * Add any Twig extensions.
     *
     * @return mixed
     */
    public function addTwigExtension()
    {
        Craft::import('plugins.positivepartnerships.twigextensions.PositivePartnershipsTwigExtension');

        return new PositivePartnershipsTwigExtension();
    }

    /**
     * Called right before your plugin’s row gets stored in the plugins database table, and tables have been created
     * for it based on its records.
     */
    public function onBeforeInstall()
    {
    }

    /**
     * Called right after your plugin’s row has been stored in the plugins database table, and tables have been
     * created for it based on its records.
     */
    public function onAfterInstall()
    {
    }

    /**
     * Called right before your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onBeforeUninstall()
    {
    }

    /**
     * Called right after your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onAfterUninstall()
    {
    }
}