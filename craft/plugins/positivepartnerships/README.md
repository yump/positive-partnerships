# Positive Partnerships plugin for Craft CMS

This plugin consists of all Positive Partnerhips Specific Functionality, created by Yump

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Positive Partnerships, follow these steps:

1. Download & unzip the file and place the `positivepartnerships` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /positivepartnerships`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `positivepartnerships` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Positive Partnerships works on Craft 2.4.x and Craft 2.5.x.

## Positive Partnerships Overview

-Insert text here-

## Configuring Positive Partnerships

-Insert text here-

## Using Positive Partnerships

-Insert text here-

## Positive Partnerships Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Yump](https://www.yump.com.au)
