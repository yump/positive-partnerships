<?php
/**
 * Positive Partnerships plugin for Craft CMS
 *
 * PositivePartnerships Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://www.yump.com.au
 * @package   PositivePartnerships
 * @since     1.0.0
 */

namespace Craft;

class PositivePartnershipsService extends BaseApplicationComponent
{
    /**
    * Get the menu in array format. Menu items are the entries in 'pages' section. 
    * @param  [EntryModel/null] $currentPage if not provided (sometimes a page might not be an entry), then the menu wouldn't contain any active state items
    * @return menu in array format. See getChildrenPages function for the format.
    */
    public function getMenu($currentPage = null) {
        $menu = array(
            //Use below to hardcode a page into the menu
            // array(
            //     'title' => 'Home',
            //     'url' => '/',
            //     'subMenu' => null,
            //     'entryId' => $currentPage ? $currentPage->id : null,
            //     'active' => ($currentPage and $currentPage->getSection()->handle == 'homepage') ? true : false,
            //     'inActivePath' => false,
            // ),
        );

        if($restOfMenuItems = $this->getChildPages($currentPage)) {
            return array_merge($menu, $this->getChildPages($currentPage));
        } else {
            return $menu;
        }
    }

    /**
     * Recursively constructing menu. Menu is based on 'pages' section entries.
     * @param  [EntryModel] $currentPage: only used for determining active or not
     * @param  [EntryModel/null] $parentPage: If $parentPage is null, consider it as root, so get all first level pages; otherwise, get the children of the given parent page.
     * @return [array/null]              return array of submenus, or null if no submenu under the given $parentPage
     *
     * Array item format (menuItem):
     *      array(
                'title' => 'string',
                'url' => 'string',
                'subMenu' => array( same format as this ) / null (if no submenu),
                'entryId' => int / null, //(used to form the menu item id for accessibility purpose)
                'active' => boolean,
                'inActivePath' => boolean, // whether one of the descendants is active
            )
     */
    public function getChildPages($currentPage, $parentPage = null) {
        $criteria = null;
        // by default, get all first level pages
        if(!$parentPage) {
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'pages';
            $criteria->level = 1;
        } elseif ($parentPage->level < 2 and $parentPage->hasDescendants()) {
            $criteria = $parentPage->getChildren();  
        }

        if($criteria) {
            $criteria->excludeFromMainMenu = "not 1";

            $menu = array();

            foreach ($criteria as $childPage) {
                $menuItem = [
                    'title' => $childPage->title,
                    'url' => $childPage->url,
                    'active' => $this->isSelfActive($currentPage, $childPage),
                    'entryId' => $childPage->id,
                ];
                $menuItem['subMenu'] = $this->getChildPages($currentPage, $childPage); // recursively construct the subMenu
                $menuItem['inActivePath'] = $this->isInActivePath($menuItem['subMenu']);

                $menu[] = $menuItem;
            }

            if(!empty($menu)) {
                return $menu;
            }
        }
    }

    public function isSelfActive($currentPage, $menuItemEntry) {
        if(!$currentPage or !$menuItemEntry) {
            return false;
        }
        return $currentPage->id == $menuItemEntry->id;
    }

    // if one of the descendants is active
    public function isInActivePath($subMenu) {
        if($subMenu) {
            foreach($subMenu as $subMenuItem) {
                if($subMenuItem['active'] or $subMenuItem['inActivePath']) {
                    return true;
                }

                if($subMenuItem['subMenu']) {
                    foreach ($subMenuItem['subMenu'] as $childSubMenuItem) {
                        if($childSubMenuItem['active'] or $childSubMenuItem['inActivePath']) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }   

    //Workshops
    // public function getWorkshops($limit = 4) {
    //     $workshops = array();

    //     $criteria = craft()->elements->getCriteria(ElementType::Entry);
    //     $criteria->section = 'workshops';
    //     $criteria->limit = $limit;

    //     return $workshops;
    // }

    /**
     * Process an array of Resources into a more suitable array format
     * Also generates Resource thumbnail images if/when needed and sends all of the * active Categories as well
     * 
     * @param array $resourceIteratables
     * @return array
     */
    private function _prepareResourceData($resourceIteratables) {
        $resourceData = [];
        $resources = [];
        $allResourceCategories = [];
        foreach ($resourceIteratables as $i => $resourceEntry) {
            $image = $resourceEntry->imageWithFocusPoint->first();
            $thumbnail = null;
            if($image) {
                $thumbnail = craft()->yump->runImager($image, array(
                    'width' => 186,
                    'height' => 218,
                    'position' => $image->focusPctX() . '% ' . $image->focusPctY() . '%',
                ));
            }

            $resourceCategories = [];
            foreach ($resourceEntry->resourceCategory as $category) {
                $resourceCategories[] = [
                  'id' => $category['id'],
                  'title' =>  $category['title']
                ];
                $allResourceCategories[$category['id']] = $category['title'];
            }

            // ddd($resourceEntry->summary);

            $summary = $resourceEntry->summary ? strip_tags($resourceEntry->summary->getRawContent()) : '';

            $resources[] = [
                'id' => $resourceEntry->id,
                'name' => $resourceEntry->title,
                'categories' => $resourceCategories,
                'summary' => $summary,
                'url' => $resourceEntry->url,
                'thumbnail' => empty($thumbnail) ? null : $thumbnail->url,
                'imageAlt' => $image ? $image->title : (empty($thumbnail) ? null : $resourceEntry->title),
            ];
        }
        $allResourceCategories = ['allCategories' => $allResourceCategories];
        $resources = ['resources' => $resources];
        $resourceData = array_merge($allResourceCategories, $resources);

        // ddd($resourceData);

        // usort($resourceData, function($a, $b) {
        //     $compare = strcasecmp($a['surname'], $b['surname']);

        //     if($compare == 0) {
        //         return 0;
        //     }

        //     return $compare > 0 ? 1 : -1;
        // });

        return $resourceData;
    }

    public function getFeaturedResources($parentPage = null, $limit = 2) {
        // dd($parentPage->url);
        if($parentPage) {
            if($parentPage->hasDescendants()) {
                $criteria = $parentPage->getChildren();  
            }
        } else {
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'pages';    
        }
        $criteria->type = 'resource';
        $criteria->order = 'postDate desc';
        $featured = [];
        $count = 0;
        foreach ($criteria as $entry) {
            if($count == $limit){
                break;
            }
            if($entry->toggle2 == '1'){
                $featured[] = $entry;
                $count++;
            }
        }
        return $this->_prepareResourceData($featured);            
    }

    public function getNotFeaturedResources($parentPage = null) {
        if($parentPage) {
            if($parentPage->hasDescendants()) {
                $criteria = $parentPage->getChildren();  
            }
        } elseif(isset($_GET['parentID'])) {
            $parentPageID = $_GET['parentID'];  
            $parentPage = craft()->entries->getEntryById($parentPageID);
            $criteria = $parentPage->getChildren();  
        } else {
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->section = 'pages';
        };
        $criteria->type = 'resource';
        $criteria->order = 'postDate desc';
        $featured = [];
        $count = 0;
        foreach ($criteria as $entry) {
            if($count == 2){
                break;
            }
            if($entry->toggle2 == '1'){
                $featured[] = $entry->id;
                $count++;
            }
        }
        if(count($featured) > 0) {
            $criteria->id = 'and, not ' . implode(', not ', $featured);
        }
        return $this->_prepareResourceData($criteria);
    }

    // public function getAllChildResources($excludingEntries = null) {
    //     $criteria = craft()->elements->getCriteria(ElementType::Entry);
    //     $criteria->section = 'pages';
    //     $criteria->type = 'resource';
    //     $criteria->limit = null;
    //     $criteria->order = 'postDate desc'; 
    //     if($excludingEntries) {
    //         $criteria->id = 'and, not ' . implode(', not ', $excludingEntries);
    //     }
    //     return $this->_prepareResourceData($criteria);
    // }

    /**
     * It's annoying that one of the filtering 'section' (resources) is part of 'pages', but different entry type, so we have to do some special thingys here to make it work. The trick is not to set 'section' as one of the criteria, but only set an array of 'entryTypes'.
     * 
     * @param  [string] $query   [description]
     * @param  [array] $filters e.g. ['pages' => 1, 'resources' => 0, 'news' => 1]
     * @return [ElementCriteriaModel]          [description]
     */
    public function search($query, $filters) {
        if(!empty($query) and !empty(trim($query))) {
            $criteria = craft()->elements->getCriteria(ElementType::Entry);
            $criteria->search = trim($query);
            $criteria->limit = null;
            $criteria->order = 'score';

            $includingEntryTypes = [];

            // handle 'pages' and 'resources'
            $pagesSection = craft()->sections->getSectionByHandle('pages');
            $pagesEntryTypes = $pagesSection->getEntryTypes();
            foreach ($pagesEntryTypes as $entryType) {
                if(!empty($filters['pages'])) { // if including 'pages'
                    if(empty($filters['resources'])) {
                        if($entryType->handle == 'resource') {
                            continue; // skip resource type
                        }
                    }
                    $includingEntryTypes[] = $entryType->id;
                } else { // if not including normal 'pages'
                    // only include resource type
                    if(!empty($filters['resources']) and $entryType->handle == 'resource') {
                        $includingEntryTypes[] = $entryType->id;
                    }
                }
            }

            // handle other types of sections
            foreach ($filters as $filterKey => $filterVal) {
                if(!empty($filterVal)) {
                    if($filterKey != 'resources' and $filterKey != 'pages') {
                        $section = craft()->sections->getSectionByHandle($filterKey);
                        if($section) {
                            $entryTypes = $section->getEntryTypes();
                            foreach ($entryTypes as $entryType) {
                                $includingEntryTypes[] = $entryType->id;
                            }
                        }
                    }
                }
            }

            // print_r($includingEntryTypes); die();

            if(!empty($includingEntryTypes)) {
                // the 'type' criteria doesn't support 'not' keyword. It only supports entry type handle, or id, or an array of those.
                $criteria->type = $includingEntryTypes;
            } else {
                // if no entryTypes included, means user unticked all filters in the front end, which should give no result. However, if we pass in empty array to ->type, it actually means: no type specified, search all entry types, which is not what we want. For an ugly workaround, we set the type to a random handle name, to make sure it will never get a result.
                $criteria->type = 'someTypeHandleWeWillNeverSet';
            }            

            return $criteria;
        }
    }

    /**
     * [formatSimplemapLocationAddress description]
     * @param  [SimpleMap_MapModel] $location [description]
     * @return [type]           [description]
     */
    public function formatSimplemapLocationAddress($location) {
        if(!empty($location->parts)) {
            $parts = $location->parts;
            // print_r($parts);
            $address = $parts['street_number'] . " " . $parts['route'] . ", " . $parts['locality'] . ", " . $parts['administrative_area_level_1_short'] . " " . $parts['postal_code'];
            return $address;
        }
    }
}