<?php
/**
 * Positive Partnerships plugin for Craft CMS
 *
 * Positive Partnerships Translation
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://www.yump.com.au
 * @package   PositivePartnerships
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
