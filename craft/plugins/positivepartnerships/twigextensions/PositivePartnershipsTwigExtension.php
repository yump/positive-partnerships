<?php
/**
 * Positive Partnerships plugin for Craft CMS
 *
 * Positive Partnerships Twig Extension
 *
 * --snip--
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and
 * functions. You can even extend the parser itself with node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://www.yump.com.au
 * @package   PositivePartnerships
 * @since     1.0.0
 */

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class PositivePartnershipsTwigExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'PositivePartnerships';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'someFilter' => new \Twig_Filter_Method($this, 'someInternalFunction'),
            'excerpt' => new \Twig_Filter_Method($this, 'positivepartnershipsGetExcerpt', array('is_safe'=>array('html'))),
        );
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'someFunction' => new \Twig_Function_Method($this, 'someInternalFunction'),
        );
    }

    /**
     * Get Excerpt 
     * @param  [string] $content - a string of content to get an excerpt from
     * @param  [number] how long should the excerpt be in characters?
     * @return string
     */
    public function positivepartnershipsGetExcerpt($content, $length = 170, $startPos = 0)
    {
        $content = strip_tags($content);
        if(strlen($content) > $length) {
            $excerpt   = substr($content, $startPos, $length-3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= '...';
        } else {
            $excerpt = $content;
        }
        return $excerpt;
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
      * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }
}