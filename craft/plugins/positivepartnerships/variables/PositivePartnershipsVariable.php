<?php
/**
 * Positive Partnerships plugin for Craft CMS
 *
 * Positive Partnerships Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://www.yump.com.au
 * @package   PositivePartnerships
 * @since     1.0.0
 */

namespace Craft;

class PositivePartnershipsVariable
{
    /**
     * Whatever you want to output to a Twig template can go into a Variable method. You can have as many variable
     * functions as you want.  From any Twig template, call it like this:
     *
     *     {{ craft.positivePartnerships.exampleVariable }}
     *
     * Or, if your variable requires input from Twig:
     *
     *     {{ craft.positivePartnerships.exampleVariable(twigValue) }}
     */
    /** 
     * If function in this class not found, go find in in service class
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(craft()->positivePartnerships, $method), $arguments);
    }  
}