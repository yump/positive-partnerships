# Yump Changelog

## 1.0.0 -- 2017-07-24

* Initial release

Brought to you by [Yump](https://yump.com.au/)
