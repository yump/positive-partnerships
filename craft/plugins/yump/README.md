# Yump plugin for Craft CMS

Custom functionalities created by Yump

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Yump, follow these steps:

1. Download & unzip the file and place the `yump` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /yump`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `yump` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Yump works on Craft 2.4.x and Craft 2.5.x.

## Yump Overview

-Insert text here-

## Configuring Yump

-Insert text here-

## Using Yump

-Insert text here-

## Yump Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Yump](https://yump.com.au/)
