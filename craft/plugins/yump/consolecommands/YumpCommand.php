<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Command
 *
 * --snip--
 * Craft is built on the Yii framework and includes a command runner, yiic in ./craft/app/etc/console/yiic
 *
 * Action methods are mapped to command-line commands, and begin with the prefix “action”, followed by
 * a description of what the method does (for example, actionPrint().  The actionIndex() method is what
 * is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft/app/etc/console/yiic yump
 *
 * The actionPrint() method above would be invoked via:
 *
 * ./craft/app/etc/console/yiic yump print
 *
 * http://spin.atomicobject.com/2015/06/16/craft-console-plugin/
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

class YumpCommand extends BaseCommand
{
    /**
     * Handle our plugin's index action command, e.g.: ./craft/app/etc/console/yiic yump
     */
    public function actionIndex($param="")
    {
    }
}