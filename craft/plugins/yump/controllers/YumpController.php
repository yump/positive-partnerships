<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

class YumpController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array(
        'actionDbBackup', 
        'actionDeleteDatedDbBackups',
    );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/yump
     */
    public function actionIndex()
    {
    }

    /**
     * Allow cron jobs to run this script to auto-backup database. 
     * - Backup file will be automatically created under /craft/storage/backup (by Craft). 
     * - A download will also be automatically triggered. 
     * - Using Yump's DB backup script for Cron job, it will be automatically saved to a Google Doc folder devs having access to.
     *
     * URL to run: [host]/actions/yump/dbBackup?a=[$PASSWORD]
     * @return null
     */
    public function actionDbBackup() {
        if ($this->_checkScriptPermission()) {
            $filePath = craft()->db->backup(); // by running this line, Craft will automatically create a backup under /craft/storage/backup
            if($filePath) {
                craft()->request->sendFile(IOHelper::getFileName($filePath), IOHelper::getFileContents($filePath), array('forceDownload' => true));
                \Yii::app()->end(); // no render
            } else {
                die('Failed to do a db backup.');
            }
        } else {
            die('You have no right to run this script, or the security settings on the server are incorrectly configured.');
        }
    }

    /** 
     * There is a setting in general.php config file (or you can create one if it's missing), to determine how long should the db backup files live on the server: 'dbBackupFilesLivespan'. This can conveniently be set for multi-environment. E.g. 30 days for live site, and 7 days for staging site.
     * @return null
     */
    public function actionDeleteDatedDbBackups() {
        if ($this->_checkScriptPermission()) {
            // trying to delete dated backup files to free up some space
            try {
                $dir = 'craft/storage/backups';
                $backupFiles = scandir($dir);

                $x = craft()->config->get('dbBackupFilesLivespan') ?: 30; // Keep the backup files for 30 days by default. Change it to w/e you like.

                // Craft use Greenwitch time to name the backup files
                $xDaysAgo = strtotime('-' . $x . ' days', time());
                // depending on how you want to compare the dates, the following line can be ignored.
                $xDaysAgo = strtotime(date('y-m-d', $xDaysAgo));

                // loop through files, find the file's last modified time and compare with $xDaysAgo
                if(!empty($backupFiles)) {
                    foreach ($backupFiles as $fileName) {
                        if(strpos($fileName, '.sql') !== false) {
                            /** Option 1: check by the modified time of the files */
                            $filepath = $dir . '/' . $fileName;
                            if(file_exists($filepath)) {
                                $created = filemtime($filepath);
                                if($created && $created < $xDaysAgo) {
                                    // print_r($filepath . ' '); // for testing
                                    unlink($filepath);
                                }
                            }

                            /** Option 2: check by the file name, which normally contains 6 digit date after the first underscore in 'ymd' format */
                            // $firstUnderscorePosition = strpos($fileName, '_');
                            // $backupDate = substr($fileName, $firstUnderscorePosition + 1, 6); // in 'ymd' format
                            // // to build a valid string format to pass into strtotime (turning into 'y-m-d' format)
                            // $formattedBackupDate = substr_replace(substr_replace($backupDate, '-', 2, 0), '-', 5, 0);

                            // $created = strtotime($formattedBackupDate);
                            // if($created && $created < $xDaysAgo) {
                            //     // print_r($dir . '/' . $fileName . ' ');
                            //     unlink($dir . '/' . $fileName);
                            // }
                        }
                    }
                }

                \Yii::app()->end(); // no render
            } catch (\Exception $e) {
               die('Failed to delete dated backup files.');
            }
        } else {
            die('You have no right to run this script, or the security settings on the server are incorrectly configured.');
        }
    }

    /**
     * Custom permission for running the controller actions. including: controllerActionsPassword, controllerActionsAllowedIps, which are set in the config file: general.php
     * @return [type] [description]
     */
    private function _checkScriptPermission($allowAdmins = false) {
        $PASSWORD = craft()->config->get('controllerActionsPassword');
        $ALLOWED_IPs = craft()->config->get('controllerActionsAllowedIps');

        return  
            // is admin
            ($allowAdmins && Craft()->getUser()->admin)
            or
            // password correct and IP is whitelisted
            (
                !empty($PASSWORD) &&
                !empty($ALLOWED_IPs) &&
                is_array($ALLOWED_IPs) &&
                craft()->request->getParam('a') == $PASSWORD && 
                in_array($_SERVER['REMOTE_ADDR'], $ALLOWED_IPs)
            ); // sometimes REMOTE_ADDR shows Cloudflare IPs if going through Cloudflare, but Cloudflare has another attribute telling us what the original IP is.

    }
}