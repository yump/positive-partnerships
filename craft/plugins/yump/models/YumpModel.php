<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

class YumpModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'someField'     => array(AttributeType::String, 'default' => 'some value'),
        ));
    }

}