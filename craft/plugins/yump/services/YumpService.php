<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

class YumpService extends BaseApplicationComponent
{
    protected $cachedCheckedPlugins = array();

   /**
     * If calling methods not found in this class, try call the one in Mobile_Detect.php
     */
    function __call($method, $arguments) {
        require_once(craft()->path->getPluginsPath() . 'yump/vendor/Mobile_Detect/Mobile_Detect.php');
        $detect = new \Mobile_Detect;
        return call_user_func_array(array($detect, $method), $arguments);
    }

    public function getIOSDevice() {
        $device = false;
        if(!empty($_SERVER['HTTP_USER_AGENT'])) {
            if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== false ) {
                $device = 'mac';
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false ) {
                $device = 'iPhone';
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false ) {
                $device = 'iPad';
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'iPod') !== false ) {
                $device = 'iPod';
            }
        }

        return $device;
    }

    public function ieVersion() {
        $version = false;
        if(!empty($_SERVER['HTTP_USER_AGENT'])) {
            if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false ) {
                $version = 6;
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') !== false ) {
                $version = 7;
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.') !== false ) {
                $version = 8;
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !== false ) {
                $version = 9;
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10.') !== false ) {
                $version = 10;
            } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false ) {
                $version = 11;
            }
        }

        return $version;
    }

    /**
     * get all entries under section (with section handle provided)
     * @param  string $sectionHandle
     * @return array of entries
     *
     * Usage example (in PHP): craft()->yump->getAllEntriesBySectionHandle($handle);
     *
     * To use in Twig template, use this:
     * craft.yump.getAllEntriesBySectionHandle($sectionHandle);
     *
     * Or use craft built-in:
     * <% craft.entries.section( $sectionHandle ).find() %>
     */
    public function getAllEntriesBySectionHandle($sectionHandle, $limit = null) {
        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->section = $sectionHandle;
        $criteria->limit = $limit;
        return $criteria->find();
    }

    /**
     * check if current user are in given group(s)
     *
     * @param  mixed $allowGroups
     * Can be string of group(s) separated by whitespaces
     * Can also be array with string of groups.
     *
     * @return bool              true / false
     *
     * Usage example:
     *  {% if not craft.yump.checkCurrentUserInGroups('organisationAdmins professionals admin') %}
            {% exit 403 %}
        {% endif %}

        This is equivlant to:
        {% if not currentUser or (not currentUser.isInGroup('organisationAdmins') and not currentUser.isInGroup('professionals') and not currentUser.admin) %}
            {% exit 403 %}
        {% endif %}
     */
    public function checkCurrentUserInGroups($allowGroups = '') {
        if(empty($allowGroups)) {
            return true;
        }
        if(is_string($allowGroups)) {
            $allowGroups = explode(' ', $allowGroups);
        }

        // get current user
        $user = craft()->userSession->getUser();

        if(empty($user)) {
            return false;
        }

        /** If $allowGroups is not array at this point, the following codes would trigger errors, and it is the way it should be, which means dev passed in wrong variable to this function */

        /** If current user is in one of the following groups, return true */
        foreach($allowGroups as $group) {
            if($user->admin AND $group === 'admin') {
                return true;
            } else {
                if($user->isInGroup($group)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check if current user has the permission to view the page
     *
     * @param  mixed $permissions
     * Can be string with permissions split by whitespace.
     * Can also be array with string of permissions.
     *
     * PLEASE NOTE: the $permissions here are the actual Craft permission handles. If custom permissions are needed, they need to be set up in plugin's main class, using registerUserPermissions() function
     *
     * @return bool              true / false
     *
     * Usage example:
     *  {% if not craft.yump.checkCurrentUserPermission('isProfessional isOrganisation') %}
            {% exit 403 %}
        {% endif %}
     */
    public function checkCurrentUserPermission($permissions = '') {
        if(empty($permissions)) {
            return true;
        }

        if(is_string($permissions)) {
            $permissions = explode(' ', $permissions);
        }

        // get current user
        $user = craft()->userSession->getUser();

        if(empty($user)) {
            return false;
        }

        /** If $permissions is not array at this point, the following codes would trigger errors, and it is the way it should be, which means dev passed in wrong variable to this function */

        /** If current user has one of the permissions, show the page (return true) */
        foreach($permissions as $permission) {
            if($user->can($permission)) {
                return true;
            }
        }

        return false;
    }

        /**
     * get the value of a matrix field, make it in php array format.
     *
     * @param  [EntryModel / UserModel] $entry can be an entry / user which has a matrix field
     * @param  [string] $matrixFieldHandle
     * @return [array]       array of matrix block with blockId => array(fieldHandle => value) format
     *
     * Example return:
     *
     * [
     *      blockId1 => [
     *          field1 => 'value1',
     *          field2 => 'value2',
     *          ...
     *      ],
     *      blockId2 => [
     *          field1 => 'valuex',
     *          field2 => 'valuey',
     *          ...
     *      ],
     *      ...
     * ]
     */
    public function getMatrixValueArray($entry, $matrixFieldHandle) {
        $matrix = $entry->$matrixFieldHandle;
        $result = [];
        foreach($matrix as $block) {
            $fields = $block->getFieldLayout()->getFields();
            $blockFieldValues = [];
            foreach ($fields as $fieldLayoutField) {
                $field = $fieldLayoutField->getField();
                $fieldHandle = $field->handle;
                $fieldType = $this->_determineFieldType($field);
                $blockFieldValues[$fieldHandle] = [
                    'type' => $fieldType,
                    'name' => $field->name,
                    'value' => $block->$fieldHandle,
                    'required' => $field->required,
                ];
            }
            $result[$block->id] = [
                'blockType' => $block->getType(),
                'blockValue' => $blockFieldValues,
            ];
        }
        return $result;
    }

    /**
     * NOTE: currently only works for text and textarea, might extend it to other types in the future.
     *
     * @param  [FieldModel] $field
     * @return [string]
     */
    private function _determineFieldType($field) {
        if($field->type == 'PlainText') {
            if(empty($field->settings['multiline'])) {
                return 'text';
            }
            else {
                return 'textarea';
            }
        }

        return '';
    }

    public function getUserGroupIdByHandle($userGroupHandle) {
        $groupId = craft()->userGroups->getGroupByHandle($userGroupHandle)->id;
        return $groupId;
    }

    public function getSectionIdByHandle($sectionHandle) {
        $sectionId = craft()->sections->getSectionByHandle($sectionHandle)->id;
        return $sectionId;
    }

    // detect if current page needs to display '...' in pagination for previous pages
    public function isFarawayToFirstPage($pageInfo, $nearbyDistance) {
        $distanceToFirstPage = count($pageInfo->getRangeUrls(1, $pageInfo->currentPage));
        if($distanceToFirstPage - $nearbyDistance > 1) {
            return true;
        } else {
            return false;
        }
    }

    // detect if current page needs to display '...' in pagination for next pages
    public function isFarawayToLastPage($pageInfo, $nearbyDistance) {
        $distanceToLastPage = count($pageInfo->getRangeUrls($pageInfo->currentPage, $pageInfo->totalPages));
        if($distanceToLastPage - $nearbyDistance > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function generateValidUrl($url) {
        if(!empty($url)) {
            $url = trim($url);
        }

        // excluding url starting with mailto:, tel:, # and /
        if(substr($url, 0, 1) == "/" or substr($url, 0, 1) == '#' or substr($url, 0, 7) == 'mailto:' or substr($url, 0, 4) == 'tel:') {
            return $url;
        }

        if(!empty($url) AND substr( $url, 0, 7 ) !== "http://" AND substr( $url, 0, 8 ) !== "https://") {
            $url = '//' . $url;
        }
        return $url;
    }

    /**
     * Surprised, we get ElementCriteriaModel instead of EntryModel if we call EntriesService->getEntryById in Twig! and the result is wrong, i guess it couldn't find the entry using the ID we supplied, (cuz the passed-in $entryId was tested as null for some reasons). Anyway, I just create this fix function for twig to use.
     * @param  [type] $entryId  [description]
     * @param  [type] $localeId [description]
     * @return [type]           [description]
     */
    public function getEntryByIdFix($entryId, $localeId = null) {
        if(is_numeric($entryId)) {
            return craft()->entries->getEntryById((int)$entryId, $localeId);
        }
    }

    /**
     * [getFlashMessage description]
     * @param  string $handle default: 'error'; also, 'notice'
     * @return String
     */
    public function getFlashMessage($handle = 'error') {
        return craft()->userSession->getFlash($handle);
    }

    public function addHttpSessionVariable($key, $value) {
        craft()->httpSession->add($key, $value);
    }

    public function isStaging() {
        return strpos($_SERVER['SERVER_NAME'], '.staging');
    }

    public function isDev() {
        return strpos($_SERVER['SERVER_NAME'], '.dev') or strpos($_SERVER['SERVER_NAME'], '.test');
    }

    public function isProduction() {
        return !$this->isStaging() AND !$this->isDev();
    }

    public function getGlobalFieldValue($globalSetHandle, $fieldHandle) {
        $settings = craft()->globals->getSetByHandle($globalSetHandle);

        if(!empty($settings)) {
            return $settings->$fieldHandle;
        } else {
            return null;
        }
    }

    /**
     * Doc: https://craftcms.com/docs/image-transforms
     *
     * This function actually just define some custom default thumbnail settings, instead of using Craft default settings. (Useless function)
     *
     * The main purpose of this function is to write this documentation...you can just use the Craft default way of doing this.........................
     *
     * Usage:
     *  {% set thumb = craft.yump.createThumbnail({
            width: 360,
        }) %}
     *
     * Sometimes it might be just easier to do it in Craft way:
     * {% set thumb = {
            mode: 'crop',
            width: 100,
            height: 100,
            quality: 75,
            position: 'top-center'
        } %}

        Here's how to use it to generate thumbnails in Twig:
        {% set thumb = {
            width: 360,
        } %}
        <img src="{{ image.getUrl(thumb) }}" width="{{ image.getWidth(thumb) }}" height="{{ image.getHeight(thumb) }}">

        image is an AssetFileModel (like the thing u get from image field value)

     * @param  [array] $settings
     */
    public function createThumbnail($settings) {
        $defaultSettings = array(
            'mode' => 'crop', // Craft default
            'width' => 500,
            'position' => 'center-center', // Craft default
        );

        if(empty($settings['mode'])) $settings['mode'] = $defaultSettings['mode'];
        if(empty($settings['width'])) $settings['width'] = $defaultSettings['width'];
        if(empty($settings['position'])) $settings['position'] = $defaultSettings['position'];
        return $settings;
    }

    /**
     * Send admin notification of server issues. Can use in caught exceptions, or caught server error events, etc.
     *
     * Recipients are defined in general.php: serverErrorEmailTo (array of emails)
     *
     * Email template is defined under yump/templates/emails/dev_notification
     *
     * variables passed into the email templates:
     * user (UserModel) - dev users
     * server (String)  - current server
     * message (String) - $message
     * request (array)  - HTTP request data, including header. Array of strings
     * vars (array)     - custom vars
     *
     * @param  string  $message         email message
     * @param  boolean $sendRequestData whether send http request data or not, including http header params
     * @param  array   $vars            custom variables - IMPORTANT! must be array of strings
     * @return null
     */
    public function sendDevErrorNotificationEmail($message, $sendRequestData = false, $vars = array()) {
        $emailTo = craft()->config->get('serverErrorEmailTo');

        // get all the recipients, defined in 'serverErrorEmailTo' in general.php (config file)
        $devs = [];
        if(!empty($emailTo)) {
            foreach ($emailTo as $devEmail) {
                $devUser = craft()->users->getUserByEmail($devEmail);
                if(!empty($devUser)) {
                    $devs[] = $devUser;
                }
            }
        }

        if(!empty($devs)) {
            $emailVars = array();
            $emailVars['message'] = $message;
            $emailVars['server'] = $_SERVER['SERVER_NAME'];
            if($sendRequestData) {
                $emailVars['request'] = $this->getHttpRequestData();
            }
            $emailVars['vars'] = $vars;

            foreach ($devs as $dev) {
                $emailVars['user'] = $dev;

                $email = new EmailModel();
                $email->toEmail = $dev->email;
                $email->subject = 'Issue found on ' . $emailVars['server'];

                // render admin_notification email template
                $oldPath = craft()->path->getTemplatesPath();
                $newPath = craft()->path->getPluginsPath().'yump/templates';
                craft()->path->setTemplatesPath($newPath);
                $email->htmlBody = craft()->templates->render('emails/dev_notification', $emailVars);
                craft()->path->setTemplatesPath($oldPath);

                craft()->email->sendEmail($email, []);
            }
        }

        return null;
    }

    /**
     * Get some useful data for server error debugging (e.g. send email notification to devs)
     * @return Array
     */
    public function getHttpRequestData() {
        $data = [];
        $requestType = craft()->request->getRequestType();
        if($requestType == 'GET') {
            $params = $_GET;
        } else if($requestType == 'POST') {
            $params = $_POST;
        } else {
            $params = craft()->request->getRestParams();
        }
        $isAjax = craft()->request->isAjaxRequest();

        $data['url'] = craft()->request->url;
        $data['requestType'] = $requestType;
        $data['ip'] = craft()->request->getIpAddress();
        $data['hostname'] = gethostbyaddr($data['ip']);
        $data['isAjax'] = $isAjax ? 'Yes' : 'No';
        $data['contentType'] = craft()->request->getMimeType();
        $data['params'] = print_r($params, true);

        $data = array_merge($data, getallheaders());

        $data['sessionVars'] = print_r($_SESSION, true);

        return $data;
    }

    public function getFieldOptions($fieldHandle) {
        $field = craft()->fields->getFieldByHandle($fieldHandle);
        $settings = $field->getAttribute('settings');
        if(!empty($settings['options'])) {
            return $settings['options'];
        } else {
            return [];
        }
    }

    public function iHaveThisOption($optionValue, $entry, $fieldHandle) {
        foreach ($entry->$fieldHandle as $option) {
            if($option->value == $optionValue) {
              return true;
            }
        }
        return false;
    }

    /**
     * get array of values / labels of the value of a multi-choice field
     * @param  EntryModel  $entry
     * @param  string  $fieldHandle
     * @param  boolean $getAssociatedArray    get associated array version of results. If this is set to true, then $getLabel becomes redundant
     * @param  boolean $getLabel    get array of labels, otherwise, get array of values
     * @return array               array of string
     */
    public function getArrayOfMultiSelectOptions($entry, $fieldHandle, $getAssociatedArray = false, $getLabel = false) {
        $result = [];
        foreach ($entry->$fieldHandle as $option) {
            if($getAssociatedArray) {
                $result[$option->value] = $option->label;
            } else {
              if ($getLabel) {
                $result[] = $option->label;
              } else {
                  $result[] = $option->value;
              }
            }

        }
        return $result;
    }

    public function newDummyEntryModel($attributes = null) {
        $entry = new EntryModel($attributes);
        return $entry;
    }

    /** return site url with protocal and server name (without back slash at the end) */
    public function getSiteUrl() {
      return sprintf(
                "%s://%s",
                isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
                $_SERVER['SERVER_NAME']
            );
    }

    public function truncate($string,$length=100,$append="...") {
      $string = trim($string);

      if(strlen($string) > $length) {
        $string = wordwrap($string, $length);
        $string = explode("\n", $string, 2);
        $string = $string[0] . $append;
      }

      return $string;
    }

    /**
     * ***************************************************************************************
     * In PHP, some functions throws notice / errors instead of exceptions.
     * If we DO want to catch those, we can use the following function. However, remember to restore the error handle when its done, no matter success or not.
     * The code to restore error handler: either use the restoreErrorHandler function underneath if can't remember, or use PHP restore_error_handler();
     *
     Example:
     set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
          // error was suppressed with the @-operator
          if (0 === error_reporting()) {
              return false;
          }

          throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
      });

      try {
          $contentArr = unserialize($content); // the code we want to make sure it wouldn't stop app
          restore_error_handler();
      } catch (\Exception $e) {
          print_r("Things go wrong!");
          restore_error_handler();
      }
     */
    public function setErrorHandler() {
      set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
          // error was suppressed with the @-operator
          if (0 === error_reporting()) {
              return false;
          }

          throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
      });
    }

    public function restoreErrorHandler() {
      restore_error_handler();
    }

    public function getGtmId() {
        // new way of getting GTM ID: via SEOMatic settings
        $gtmId = $this->getSiteIdentityAttribute('googleTagManagerID');
        if($gtmId) return $gtmId;

        // if not set, try to get it from the old way: defined in globals

        // old way of getting GTM IDs
        $matrix = craft()->yump->getGlobalFieldValue('externalScripts', 'externalScripts');

        if($matrix) {
          foreach ($matrix as $block) {
            if($block->type == 'googleTagManager' AND !empty(trim($block['googleTagManagerId']))) {
              return trim($block['googleTagManagerId']);
            }
          }
        }
    }

    public function getMicrotimeAsId() {
        return str_replace(" ", "-", str_replace(".", "-", microtime()));
    }

    public function getSectionContainerClass($block) {
        if(!empty($block['sectionLayout'])) {
            $layout = $block->sectionLayout;
            if($layout == 'boxed') {
                return 'container';
            } elseif($layout == 'fluid') {
                return 'container-fluid';
            } elseif($layout == 'fullWidth') {
                return 'container-full-width';
            }
        }
        return null;
    }

    public function getSectionTopBottomPaddingClass($block) {
        if(!empty($block['topAndBottomPadding'])) {
            $paddingType = $block->topAndBottomPadding;
            if($paddingType == 'no-top') {
                return 'section-padding-no-top';
            } elseif($paddingType == 'no-bottom') {
                return 'section-padding-no-bottom';
            } elseif($paddingType == 'none') {
                return 'section-padding-no-top-no-bottom';
            }
        }
        return null;
    }

    public function getFirstname($fullName) {
      $nameArr = explode(' ', $fullName, 2);
      if(count($nameArr) >= 1) {
        return $firstName = $nameArr[0];
      }
    }

    /**
     * insert ',' into numbers
     */
    public function formatNumber($number) {
        if(!$number) return $number;
        return number_format($number);
    }

    public function hexToRgb($hex, $returnAsArray = false) {
        try{
            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
            if($r && $g && $b) {
                if($returnAsArray) {
                    return [
                        'R' => $r,
                        'G' => $g,
                        'B' => $b,
                    ];
                } else {
                    return $r . ', ' . $g . ', ' . $b;
                }
            }
        } catch (\Exception $e) {}
    }

    public function isInCategory($element, $categoryFieldHandle, $categorySlug) {
        if(!empty($element[$categoryFieldHandle])) {
            foreach($element[$categoryFieldHandle] as $category) {
                if($category->slug == $categorySlug) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getTelHref($number) {
        $temp = str_replace(' ', '-', trim($number));
        $temp = str_replace('(', '', $temp);
        return str_replace(')', '', $temp);
    }

    /**
     * Check if a plugin has been loaded and enabled, and cached the result in this class. But it only caches it if it is already loaded and enabled, otherwise, it will check again next time when called.
     * @param  [type]  $pluginHandle [description]
     * @return boolean               true / false
     */
    public function isPluginLoaded($pluginHandle) {
        if(!empty($this->cachedCheckedPlugins[$pluginHandle])) {
            return true;
        } else {
            $plugin = craft()->plugins->getPlugin($pluginHandle);
            if($plugin) {
                $this->cachedCheckedPlugins[$pluginHandle] = true;
                return true;
            }
        }

        return false;
    }

    /**
     * SEOmatic data helper
     * get the social media handle (username), given the name of social media. Social settings are set up in SEOmatic
     * @param  [string] $socialMediaName: Strictly restricted to the following strings (case sensitive):
     * 'facebook', 'twitter', 'linkedIn', 'googlePlus', 'youtube', 'youtubeChannel', 'instagram', 'pinterest', 'github', 'vimeo'
     * @return [string/null]
     */
    public function getSocialMediaHandle($socialMediaName) {
        if($this->isPluginLoaded('seomatic')) {
            $socialSettings = craft()->seomatic->getSocial(craft()->language);
            if(!empty($socialSettings[$socialMediaName . 'Handle'])) {
                return $socialSettings[$socialMediaName . 'Handle'];
            }
        }
    }

    /**
     * SEOmatic data helper
     * get one specific attribute set up in SEOmatic's Site Identity section
     * @param  [string] $attr [description]
     * @return [mixed/null]       [description]
     */
    public function getSiteIdentityAttribute($attr) {
        if($this->isPluginLoaded('seomatic')) {
            $identity = craft()->seomatic->getIdentity(craft()->language);
            if(!empty($identity[$attr])) {
                return $identity[$attr];
            }
        }
    }

    /**
     * Safe way to run Imager plugin.
     * If imager is correctly installed it will call that, otherwise
     * will return default URL.
     *
     * @param EntryObject $content
     * @param $config
     * @return string - the new URL of the resized image (or original URL if Imager isn't loaded)
     */
    public function runImager($content, $config) {
        if(empty($content) or !($content instanceof AssetFileModel)) {
            return null;
        }

        /** just return the URL if it's SVG */
        if(stripos($content->getMimeType(), 'svg') !== FALSE) {
            return $content->getUrl();
        }

        $imagerPluginLoaded = $this->isPluginLoaded('imager');
        if($imagerPluginLoaded) {
            try {
                // echo "Running imager.";
                // echo $content->getUrl($config);
                return craft()->imager->transformImage($content, $config, null, null);
            } catch(\Exception $e) {
                // echo $e->getMessage();
            }
        }
        // echo "Imager not loaded.";
        return $content->getUrl($config);
    }

    public function highlight($c,$q)
    {
        $q = str_replace(array('','\\','+','*','?','[','^',']','$','(',')','{','}','=','!','<','>','|',':','#','-','_'),'',$q);
        $c=preg_replace("/($q)(?![^<]*>)/i","<span class=\"highlight\">\${1}</span>",$c);
        // $q=explode(' ',str_replace(array('','\\','+','*','?','[','^',']','$','(',')','{','}','=','!','<','>','|',':','#','-','_'),'',$q));
        // for($i=0;$i<sizeOf($q);$i++)
        //     $c=preg_replace("/($q[$i])(?![^<]*>)/i","<span class=\"highlight\">\${1}</span>",$c);
        return $c;
    }

    public function searchExcerpt($text, $phrase, $radius = 150, $ending = "..."){

        //$text =  preg_replace("/<img[^>]+\>/i", "", $text);
        $text = strip_tags($text);

        $phraseLen = strlen($phrase);

        //don't let the radius be less than the phrase that searched for
        if ($radius < $phraseLen) {
             $radius = $phraseLen;
        }
        $phrases = explode (' ',$phrase);

        //search for instances
         foreach ($phrases as $phraseUnit) {
             $pos = strpos(strtolower($text), strtolower($phraseUnit));
             if ($pos > -1) {
                break; }
                else {
                    return false;
                }
         }

         $startPos = 0;
         if ($pos > $radius) {
             $startPos = $pos - $radius;
         }

         $textLen = strlen($text);

         $endPos = $pos + $phraseLen + $radius;
         if ($endPos >= $textLen) {
             $endPos = $textLen;
         }

         $excerpt = substr($text, $startPos, $endPos - $startPos);
         if ($startPos != 0) {
             $excerpt = substr_replace($excerpt, $ending, 0, $phraseLen);
         }

         if ($endPos != $textLen) {
             $excerpt = substr_replace($excerpt, $ending, -$phraseLen);
         }

         //highlight
         $excerpt = $this->highlight($excerpt, $phrase);

         return $excerpt;
    }

    public function deepBodySearch($query, $object, $fieldsToSearch = array('richText', 'summary', 'richTextSummary')) {

        $elementType = @$object->getElementType();
        if($elementType) {
            if($elementType == ElementType::Entry) {
                $block = @$object['body'];
            } else {
                $block = $object;
            }
            if($block) {
                if($block instanceof Neo_BlockModel or $block instanceof MatrixBlockModel) {
                    foreach ($fieldsToSearch as $field) {
                        if(!empty($block[$field])) {
                            $found = $this->searchExcerpt($block[$field], $query);
                            if($found) {
                                return $found;
                            }
                        }
                    }
                }

                if($block instanceof Neo_CriteriaModel) {
                    foreach ($block as $childBlock) {
                        $found = $this->deepBodySearch($query, $childBlock, $fieldsToSearch);
                        if($found) {
                            return $found;
                        }
                    }
                }

                if($block instanceof Neo_BlockModel and !empty($block['reusableSnippet'])) {
                    $found = $this->deepBodySearch($query, $block['reusableSnippet'], $fieldsToSearch);
                    if($found) {
                        return $found;
                    }
                }

                /** This compactContentMatrix is used in Healthecare, it's unlikely to be used in future projects. However, you can replace this with any matrix field put in Neo if you want the search excerpt to work on the content of this matrix field as well. In the future at some point, maybe we can loop through the fields of a Neo_BlockModel and handle each type separately. */
                if($block instanceof Neo_BlockModel and !empty($block['compactContentMatrix'])) {
                    $found = $this->deepBodySearch($query, $block['compactContentMatrix'], $fieldsToSearch);
                    if($found) {
                        return $found;
                    }
                }

                if($block instanceof ElementCriteriaModel and $block->getElementType()->getClassHandle() == 'MatrixBlock') {
                    foreach ($block as $childMatrixBlock) {
                        if($childMatrixBlock instanceof MatrixBlockModel) {
                            $found = $this->deepBodySearch($query, $childMatrixBlock, $fieldsToSearch);
                            if($found) {
                                return $found;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function getLastWordInString($string) {
        $pieces = explode(' ', $string);
        return array_pop($pieces);
    }

    public function getVideoId($videoUrl) {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/craft/plugins/videoembedutility/twigextensions/VideoEmbedUtilityTwigExtension.php";
        if(file_exists($path)) {
            try{
                require_once($path);
                $helper = new VideoEmbedUtilityTwigExtension();
                return $helper->videoId(trim($videoUrl));
            } catch (\Exception $e) {}
        }
    }

    /**
     * Check if a hash string is valid in URL
     * @param  [string] $hash
     * @return [trimmed URL | false if invalid]
     */
    public function validateUrlHash($hash) {
        $hash = trim($hash);
        $testUrl = 'http://superdummytest.com#' . $hash;
        if(filter_var($testUrl, FILTER_VALIDATE_URL) === false) {
            return false;
        }

        return $hash;
    }

    public function getYumpThumbnailerUrl($url, $config) {
        $url = str_replace('https://', '/https/', $url);
        $url = str_replace('http://', '/http/', $url);
        $url = str_replace('//', '/', $url);
        $url = trim($url);

        $mode = !empty($config['mode']) ? $config['mode'] : 'crop';
        $width = !empty($config['width']) ? $config['width'] : 9999;
        $height = !empty($config['height']) ? $config['height'] : 9999;

        return "/thumb/" . $width . "x" . $height . "/" . $mode . $url;
    }


    public function isNavItemActive($element) {
        if(!empty($element)) {
            $segments = craft()->request->getSegments();
            $slug = $element->slug;
            if($slug and in_array($slug, $segments)) {
                return true;
            }
        }
        return false;
    }

    /**
     * craft()->sections->getEntryTypesByHandle returns an array instead of just one. My guess is there could be entry types in different sections using the same handle. So, to get a unique entry type, we pass in a second param '$sectionHandle' to pinpoint one entry type model
     * @param  [type] $handle        [entry type handle]
     * @param  [type] $sectionHandle [section handle]
     * @return [EntryTypeModel|null]                [description]
     */
    public function getEntryTypeByHandle($handle, $sectionHandle) {
        $entryTypes = craft()->sections->getEntryTypesByHandle($handle);
        if(!empty($entryTypes)) {
            foreach ($entryTypes as $entryType) {
                $section = $entryType->getSection();
                if($section and $section->handle == $sectionHandle) {
                    return $entryType;
                }
            }
        }
    }

    public function getCategoriesByGroupHandle($categoryGroupHandle, $returnTitlesOnly = false) {
        $criteria = craft()->elements->getCriteria(ElementType::Category);
        $criteria->group = $categoryGroupHandle;
        $criteria->limit = null;
        $categories = $criteria->find();
        if(!$returnTitlesOnly) {
            return $categories;
        } else {
            $categoryTitles = [];
            foreach ($categories as $category) {
                $categoryTitles[] = $category->title;
            }
            return $categoryTitles;
        }
    }

    /**
     * Extract (from Mailchimp form embedded code) the form action URL string and construct the bot hidden field value string based on that, so the admin only need to copy & paste in the entire embedded code without manually extract the two strings into CMS.
     * @return array|null if fail.
     * Example: array('action' => 'https://yump.us3.list-manage.com/subscribe/post?u=e4429d6397aaf2320b515bb93&amp;id=d68e372e21', 'botId' => 'b_e4429d6397aaf2320b515bb93_d68e372e21')
     */
    public function extractMailchimpFormData($embeddedCode) {
        $matches = array();
        preg_match('/<form action="(((?!").)+)"/', $embeddedCode, $matches);

        if(!empty($matches[1]) and strpos($matches[1], '"') === false) {
            $action = $matches[1];
            $m = [];
            preg_match('/u=([a-zA-Z0-9]+)&amp;id=([a-zA-Z0-9]+)/', $action, $m);

            if(!empty($m[1]) and !empty($m[2])) {
                $botId = 'b_' . $m[1] . '_' . $m[2];
                return [
                    'action' => $action,
                    'botId' => $botId,
                ];
            }
        }
    }

    /**
     * Craft put this 'p' param in the URL, we probably want to kill that to make the URL cleaner.
     * @return string: constructed URL params. E.g. query=test&location=vic
     */
    public function getUrlParams() {
        $paramString = '';
        $usableParams = [];
        foreach ($_GET as $key => $value) {
            if($key != 'p') {
                $usableParams[] = $key . '=' . $value;
            }
        }
        $paramString = implode('&', $usableParams);

        return $paramString;
    }

    /**
     * The craft's craft.request.getSegments only works for current URL. But this function use URL as a param, so you can get segments of any URL.
     * @param  [string] $url [description]
     * @return [type]      [description]
     */
    public function getUrlSegments($url) {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        return explode('/', $uri_path);
    }

    /**
     * Can get events either from twig: craft.yump.getEventbriteEvents(), or via Ajax: /actions/yump/eventbrite/getEvents in Yump_EventbriteController
     * @return [array] events
     */
    public function getEventbriteEvents() {
        return craft()->yump_eventbrite->getEvents();
    }

    /**
     * Get excerpt from string
     *
     * @param String $str String to get an excerpt from
     * @param Integer $startPos Position int string to start excerpt from
     * @param Integer $maxLength Maximum length the excerpt may be
     * @return String excerpt
     */
    public function getExcerpt($str, $startPos=0, $maxLength=100) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength-3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= '...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    /**
     * Quick helper function to validate an element's content (field data)
     *
     * IMPORTANT! Avoid using this function if the element is going to be saved in the same request (instance), because Craft's saveElement code already handles validation. By doing that, we might end up duplicating the errors for one field. This function is mainly used for a single one request for validation purpose only. E.g. front-end ajax saveEntry form
     * @param  [BaseElementModel or anything extend it, e.g. $entry, $user, $category, etc] $element
     * @return [bool]          valid or not. you can then call $element->getErrors() to get all the errors if necessary.
     */
    public function validateElementContent($element) {
        $oldFieldContext = craft()->content->fieldContext;
        $oldContentTable = craft()->content->contentTable;

        craft()->content->fieldContext = $element->getFieldContext();
        craft()->content->contentTable = $element->getContentTable();

        $valid = craft()->content->validateContent($element);
        $element->addErrors($element->getContent()->getErrors()); // so we can call $element->getErrors() to get the errors

        craft()->content->fieldContext = $oldFieldContext;
        craft()->content->contentTable = $oldContentTable;

        return $valid;
    }

	/**
	 * VideoEmbed plugin is not accessible, so we create our own plugin to add custom 'title' attribute based in the iframe tab
	 * @param  [mixed] $inputBlock can be either: 1. string - video url; 2. any object which has these two fields: videoUrl(required) and videoTranscript(optional)
	 * @param  array  $options    [description]
	 * @return [type]             [description]
	 */
	public function videoEmbed($inputBlock, $options = array()) {
		$path = $_SERVER['DOCUMENT_ROOT'] . "/craft/plugins/videoembedutility/twigextensions/VideoEmbedUtilityTwigExtension.php";

		if(gettype($inputBlock) === 'string') {
			$videoUrl = trim($inputBlock);
		} else {
			$videoUrl = @$inputBlock['videoUrl'];
		}
		if(file_exists($path) and !empty($videoUrl)) {
			try{
				require_once($path);
				$helper = new VideoEmbedUtilityTwigExtension();
				$width = '100%';
				$height = '148';
				$url = $helper->videoPlayerUrl(trim($videoUrl));

				if(!empty($url)) {
					if(!empty($options)) {
						if(isset($options['width'])) {
							$width = $options['width'];
							unset($options['width']);
						}

						if(isset($options['height'])) {
							$height = $options['height'];
							unset($options['height']);
						}

						$url .= '&' . http_build_query($options);
					}

					$originalPath = craft()->path->getTemplatesPath();
					$myPath = craft()->path->getPluginsPath() . 'yump/templates/integrations/videoembed/';
					craft()->path->setTemplatesPath($myPath);
					$renderOptions = array(
						'player_url' => $url,
						'width' => $width,
						'height' => $height,
					);
					if(!empty($inputBlock['videoTranscript']) and $inputBlock->videoTranscript->first()) {
						$renderOptions['videoTitle'] = $inputBlock->videoTranscript->first()->title;
					}
					$markup = craft()->templates->render('_videoEmbed.html', $renderOptions);
					craft()->path->setTemplatesPath($originalPath);
					return TemplateHelper::getRaw($markup);
				}
			} catch (\Exception $e) {}
		}
	}
}
