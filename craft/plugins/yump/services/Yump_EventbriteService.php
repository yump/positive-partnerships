<?php
/**
 * mackillop plugin for Craft CMS
 *
 * Mackillop Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2018 Yump
 * @link      https://yump.com.au
 * @package   Mackillop
 * @since     1.0.0
 */

namespace Craft;

class Yump_EventbriteService extends BaseApplicationComponent
{
    /**
     * For debugging and all the params we can pass into the API:
     * https://www.eventbriteapi.com/v3/events/search/?token=[token]&sort_by=-date&user.id=[userId]
     * @return [type] [description]
     */
    public function getEvents()
    {
        $events = [];

        $token = craft()->config->get('eventbriteOauthToken');
        $userId = craft()->config->get('eventbriteUserId');
        // $response = $this->_getRequest('users/me', $token); // testing to get the userId

        $responses = [];
        if(!empty($token) and !empty($userId)) {
            do {
                $params = array(
                    'token' => $token,
                    'sort_by' => '-date',
                    'user.id' => $userId,
                );
                if(!empty($pagination['continuation'])) {
                    $params['continuation'] = $pagination['continuation'];
                }
                $response = $this->_getRequest('events/search', $token, $params);
                $responses[] = $response;
                if(!empty($response['success']) and !empty($response['JSON']['pagination']) and !empty($response['JSON']['events'])) {
                    $events = array_merge($events, $response['JSON']['events']);
                    $pagination = $response['JSON']['pagination'];
                } else {
                    error_log(print_r($response, true));
                    break;
                }
            } while (!empty($pagination['has_more_items']) and !empty($pagination['continuation']) and count($events) < 200);
        }

        return $events;
    }

    private function _getRequest($endpoint, $token, $params = [], $cacheTimeInMinutes = 15) {
        $endpoint = trim($endpoint, '/') . '/';
        $url = "https://www.eventbriteapi.com/v3/" . $endpoint;
        if(!empty($params)) {
            $paramStrings = [];
            foreach ($params as $key => $value) {
                $paramStrings[] = $key . "=" . $value;
            }
            $url .= '?' . implode('&', $paramStrings);
        }
        return craft()->yump_curl->curl($url, array(
            'cacheFolder' => craft()->config->get('yumpCurlCacheFolder'),
            'cacheTimeInMinutes' => $cacheTimeInMinutes,
            // 'headers' => [
            //     // "Authorization: Bearer " . $token,
            //     // "Content-type: application/json",
            // ],
        ));
    }
}