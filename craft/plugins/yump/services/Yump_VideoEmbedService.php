<?php
/**
 * The VideoEmbed plugin's twig methods has some issues. E.g. not accessibility friendly (with no title attribute), also, it forces YouTube params to be controls=2, which doesn't allow client to put in custom params like rel=0, which is critical to disable related videos after it finishes playing, because related videos can come from client's competitors.
 *
 * So, we created this class, which is mostly a copy of VideoEmbed's VideoEmbedUtilityTwigExtension.php, but have our custom code to overcome those issues.
 *
 * UPDATES: actually, I later on found out the extra params can be pass in as an option in template. E.g. {{ videoUrl | trim | videoEmbed({ rel: 0 }) }}. So this biggest issue can be easily resolved by this. Anyway, we can still keep this class for MacKillop because it's important to have the 'title' attr for accessibility.
 */

namespace Craft;

defined("VIMEO") or define("VIMEO",'vimeo.com');
defined("YOUTUBE") or define("YOUTUBE",'youtube.com');
defined("YOUTUBE_SHORT") or define("YOUTUBE_SHORT",'youtu.be');
defined("FACEBOOK") or define("FACEBOOK",'facebook.com');

class Yump_VideoEmbedService extends BaseApplicationComponent
{
    private static $KNOWN_HOSTS = array(VIMEO,YOUTUBE,YOUTUBE_SHORT,FACEBOOK);
        
    /**
     * Returns a string indicating where this video is hosted (youtube, vimeo, etc.)
     *
     * @param string $videoUrl
     * @return string
     */
    public function videoHost($videoUrl) {
        $host = parse_url($videoUrl, PHP_URL_HOST);
        // return a sanitized value (no leading www, etc) if it's one we know.
        foreach($this::$KNOWN_HOSTS as $known) {
            if( strpos($videoUrl,$known) !== FALSE ) {
                return $known;
            }
        }
        return $host;
    }
            
    public function videoId($videoUrl) {
        $host = $this->videoHost(trim($videoUrl));
        switch($host) {
            case VIMEO:
                if(preg_match('/\/([0-9]+)\/*(\?.*)?$/',$videoUrl,$matches) !== false) {
                    return $matches[1];
                }
            break;
            
            case YOUTUBE:
            case YOUTUBE_SHORT:
                if(preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',$videoUrl,$matches) !== false)
                    return $matches[1];
            break;
        }
        return "";
    }
    
    public function videoPlayerUrl($input) {
        $videoId = $this->videoId($input);
        switch($this->videoHost($input)) {
            case VIMEO:
                return "//player.vimeo.com/video/$videoId?";
            break;
            
            case YOUTUBE:
            case YOUTUBE_SHORT:
                return "//www.youtube.com/embed/$videoId?controls=2&rel=0";
            break;

            case FACEBOOK:
                return '//www.facebook.com/plugins/video.php?href=' . urlencode($input) . '&show_text=0';
            break;
        }
        return "";
    }
    
    /**
    * Returns a boolean indicating whether the string $haystack ends with the string $needle.
    * @param string $haystack the string to be searched
    * @param string $needle the substring we're looking for
    * return boolean
    */
    private function endsWith($haystack, $needle) {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }
    
    public function videoEmbed($input, $options = array()) {
        if(gettype($input) === 'string') {
            $videoUrl = trim($input);
        } else {
            $videoUrl = @$input['videoUrl'];
        }

        $width = '100%';
        $height = '148';
        $url = $this->videoPlayerUrl($input);
        $videoTitle = null;
        
        if(!empty($url)) {
            if(!empty($options)) {
                if(isset($options['width'])) {
                    $width = $options['width'];
                    unset($options['width']);
                }
                
                if(isset($options['height'])) {
                    $height = $options['height'];
                    unset($options['height']);
                }

                if(isset($options['videoTitle'])) {
                    $videoTitle = $options['videoTitle'];
                    unset($options['videoTitle']);
                }

                if(!empty($options)) {
                    $url .= '&' . http_build_query($options);
                }
            }
            
            $originalPath = craft()->path->getTemplatesPath();
            $myPath = craft()->path->getPluginsPath() . 'yump/templates/integrations/videoembed/';
            craft()->path->setTemplatesPath($myPath);
            $renderOptions = array(
                'player_url' => $url,
                'width' => $width,
                'height' => $height,
                'videoTitle' => $videoTitle,
            );
            
            $markup = craft()->templates->render('_videoEmbed.html', $renderOptions);
            craft()->path->setTemplatesPath($originalPath);
            return TemplateHelper::getRaw($markup);
        }
    }
}
