<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Translation
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
    "What do you want to do with their content?" => "What do you want to do with the content (including all the pages, categories, etc) created by them? If you just delete it, this content will be gone permanently.",
);
