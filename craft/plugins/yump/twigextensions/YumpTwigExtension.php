<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Twig Extension
 *
 * --snip--
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and
 * functions. You can even extend the parser itself with node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class YumpTwigExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'Yump';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'someFilter' => new \Twig_Filter_Method($this, 'someInternalFunction'),
            'yumpmin' => new Twig_Filter_Method($this, 'yumpAutominFilter', array('is_safe'=>array('html'))),
            'yumpsass' => new Twig_Filter_Method($this, 'yumpSassFilter', array('is_safe'=>array('html'))),
            'yumpcss' => new Twig_Filter_Method($this, 'yumpCssFilter', array('is_safe'=>array('html'))),
            'yumpjs' => new Twig_Filter_Method($this, 'yumpJsFilter', array('is_safe'=>array('html'))),

            'yumpimager' => new Twig_Filter_Method($this, 'yumpImagerFilter', array('is_safe'=>array('html'))),

            'searchExcerpt' => new \Twig_Filter_Method($this, 'searchExcerpt'),
            'getExcerpt' => new \Twig_Filter_Method($this, 'getExcerpt'),

            'yumpVideoId' => new \Twig_Filter_Method($this, 'videoId', array('is_safe'=>array('html'))),
            'yumpVideoHost' => new \Twig_Filter_Method($this, 'videoHost', array('is_safe'=>array('html'))),
            'yumpVideoPlayerUrl' => new \Twig_Filter_Method($this, 'videoPlayerUrl', array('is_safe'=>array('html'))),
            'yumpVideoEmbed' => new \Twig_Filter_Method($this, 'videoEmbed', array('is_safe'=>array('html'))),
        );
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'someFunction' => new \Twig_Function_Method($this, 'someInternalFunction'),
        );
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
      * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }

    /**
     * We extend the Automin plugin to apply filter directly on a string of filepath, or an array of strings of filepath, by adding an additional parameter ($filePathsAsContent) into the process function in AutominService.php. By doing this, we also sacrifies the $attr param, which is custom attributes defined, because I think it's rarely use on including css / js files anyway. For all css files, we pre-define the rel="stylesheet" attribute by default.
     * @param  [string/array] $content - a string of filepath, or an array of strings of filepath
     * @param  [string] $type - 'css', 'less', 'scss', or 'js'; I don't think anything else are supported by Automin
     * @return compile and minify all the including files, cached as one single file. Output the HTML element to include the file. E.g.
     * <link href="/minified/430db7f50180f8af9e1e378b21253aa9.css?modified=1503448076" rel="stylesheet">
     * <script type="text/javascript" src="/minified/cc03ebb57b04228284176d04eaf13d3b.js?modified=1501121493"></script>
     *
     * Usage example:
     * 1. One single file: {{ "/src/scss/core/compact-page.scss" | yumpmin('scss') }}
     *    Output: <link href="/minified/430db7f50180f8af9e1e378b21253aa9.css?modified=1503448076" rel="stylesheet">
     * 2. Multiple files:
     *  {{ [
            "/yump/scss/YumpMagicCSS.scss",
            "/src/scss/core/blocks-basic.scss",
            "/src/scss/styles.scss"
        ] | yumpmin('scss') }}
        Output: <link href="/minified/430db7f50180f8af9e1e378b212sfe23.css?modified=1505558075" rel="stylesheet">

        IMPORTANT:
        Use 'scss' on only the scss file(s), 'css' only on css file(s). Using the filter on a wrong time would ignore the mismatching-type files
     */
    public function yumpAutominFilter($content, $type)
    {
        $attr = '';
        if($type == 'css' OR $type == 'scss' OR $type == 'less') {
            $attr = 'rel="stylesheet"';
        }
        return craft()->automin->process($content, $type, $attr, true);
    }

    /**
     * This filter extends the 'yumpmin' filter above, to give devs more convenient way of using the filter to include sass files. In addition, it also supports returning filepath only instead of the whole HTML element.
     * @param  see yumpAutominFilter
     * @param  boolean $returnFilePathOnly - indicating returning filepath only or the whole HTML element. Outputing HTML element by default (same as 'yumpmin' and 'automin')
     * @return see yumpAutominFilter
     *
     * 1. One single file:
     * {{ "/src/scss/core/compact-page.scss" | yumpsass }}
     * 2. Multiple files:
     *  {{ [
            "/yump/scss/YumpMagicCSS.scss",
            "/src/scss/core/blocks-basic.scss",
            "/src/scss/styles.scss"
        ] | yumpsass }}

        The two examples above would output things like this:
        <link href="/minified/430db7f50180f8af9e1e378b21253aa9.css?modified=1503448076" rel="stylesheet">

       3. Get compiled and cached file instead:
        {% includeCssFile "/src/scss/core/carousel.scss" | yumpsass(true) %}

        IMPORTANT:
        Only use it on scss file(s)

        See _layout.html for general rules to include CSS / JS files
     */
    public function yumpSassFilter($content, $returnFilePathOnly = false) {
        /** automatically include the fundamental sass files (e.g. variables) every time we use the yumpsass filter */
        if($settings = craft()->config->get('preImportSassFiles')) {
            if(!is_array($settings)) {
                $settings = [$settings];
            }
            if(!is_array($content)) {
                $content = [$content];
            }
            $content = array_merge($settings, $content);
        }

        $htmlOutput = $this->yumpAutominFilter($content, 'scss');
        if($returnFilePathOnly) {
            $pattern = "/href\=\"([A-Za-z0-9\.\/\_\-\?\=\:]+\\.css[A-Za-z0-9\.\/\_\-\?\=\:]*)\"/";
            $match = $this->_findMatchedPattern($pattern, $htmlOutput);
            if ($match) {
                return $match;
            }
        }
        return $htmlOutput;
    }


    public function yumpCssFilter($content, $returnFilePathOnly = false) {
        $htmlOutput = $this->yumpAutominFilter($content, 'css');
        if($returnFilePathOnly) {
            $pattern = "/href\=\"([A-Za-z0-9\.\/\_\-\?\=\:]+\\.css[A-Za-z0-9\.\/\_\-\?\=\:]*)\"/";
            $match = $this->_findMatchedPattern($pattern, $htmlOutput);
            if ($match) {
                return $match;
            }
        }
        return $htmlOutput;
    }

    public function yumpJsFilter($content, $returnFilePathOnly = false) {
        $htmlOutput = $this->yumpAutominFilter($content, 'js');
        if($returnFilePathOnly) {
            $pattern = "/src\=\"([A-Za-z0-9\.\/\_\-\?\=\:]+\\.js[A-Za-z0-9\.\/\_\-\?\=\:]*)\"/";
            $match = $this->_findMatchedPattern($pattern, $htmlOutput);
            if ($match) {
                return $match;
            }
        }
        return $htmlOutput;
    }

    /**
     * Even though we suppress the imager's exceptions, it still throws some of the exceptions. So, we created this custom filter to use imager, by suppressing all the exceptions. Also, this filter will use Craft's native image transform function if imager doesn't work.
     * @param  [image object (AssetFileModel) | null] $content
     * @param  [array] $config  imager's transform parameters, see https://github.com/aelvan/Imager-Craft#transform-parameters
     * @return null if image is null, or Craft's native image transform
     *
     * Usage example:
     * {% set image = entry.imageHandle.first() %}
     * {% set thumbnail = image | yumpimager({width: 600}) %}
     *
     * To use together with Focus Point to crop the image (don't need this if using Focus Point to set CSS background position):
     * {% set image = entry.imageHandle.first() %}
       {% if image %}
            {% set thumbnail = image | yumpimager({
                width: 300,
                height: 300,
                position: image.focusPctX ~ '% ' ~ image.focusPctY ~ '%',
            }) %}
        {% else %}
            {% set thumbnail = "/src/img/placeholder.jpg" %}
        {% endif %}
     */
    public function yumpImagerFilter($content, $config) {
        return craft()->yump->runImager($content, $config);
    }

    private function _findMatchedPattern($pattern, $string) {
        $matches;
        preg_match($pattern, $string, $matches);
        if (count($matches) >= 2) {
            return $matches[1];
        }
        return false;
    }

    public function searchExcerpt($text, $phrase, $radius = 150, $ending = "..."){
        return craft()->yump->searchExcerpt($text, $phrase, $radius, $ending);
    }

    public function getExcerpt($text, $options = array()) {
        $options = array_merge(array(
            'startPos' => 0,
            'maxLength' => 150,
        ), $options);

        return craft()->yump->getExcerpt($text, $options['startPos'], $options['maxLength']);
    }

	public function videoId($inputBlock, $options = array()) {
		return craft()->yump_videoEmbed->videoId($inputBlock, $options);
	}

	public function videoHost($inputBlock, $options = array()) {
		return craft()->yump_videoEmbed->videoHost($inputBlock, $options);
	}

	public function videoPlayerUrl($inputBlock, $options = array()) {
		return craft()->yump_videoEmbed->videoPlayerUrl($inputBlock, $options);
	}

	/**
	 * Override the VideoEmbed plugin's videoEmbed filter, to have our custom template and data.
	 * @param  [mixed] $inputBlock [$inputBlock can be either: 1. string - video url; 2. any object which has these two fields: videoUrl(required) and videoTranscript(optional)]
	 * @param  array  $options    [description]
	 * @return [type]             [description]
	 */
	public function videoEmbed($inputBlock, $options = array()) {
		return craft()->yump_videoEmbed->videoEmbed($inputBlock, $options);
	}
}
