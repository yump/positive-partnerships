<?php
/**
 * Yump plugin for Craft CMS
 *
 * Yump Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    Yump
 * @copyright Copyright (c) 2017 Yump
 * @link      https://yump.com.au/
 * @package   Yump
 * @since     1.0.0
 */

namespace Craft;

class YumpVariable
{
    /** 
     * If function in this class not found, go find in in service class
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(craft()->yump, $method), $arguments);
    }  
}