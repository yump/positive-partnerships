<?php
namespace Craft;

class YumpFormPlugin extends BasePlugin
{
    function getName()
    {
         return Craft::t('Yump Form');
    }

    function getVersion()
    {
        return '1.0';
    }

    function getDeveloper()
    {
        return 'Yump';
    }

    function getDeveloperUrl()
    {
        return 'http://www.yump.com.au';
    }

    // function onAfterInstall() {

    // }

    // function onBeforeInstall() {

    // }

    // function onBeforeUninstall() {
        
    // }
    
    function init() {
        // craft()->on('entries.onBeforeSaveEntry', function(Event $event) {
        //     $entry = $event->params['entry'];
        //     $postContent = $entry->getContentFromPost();
            
        //     foreach ($postContent as $fieldHandle => $value) {
        //         $fieldType = craft()->fields->getFieldByHandle($fieldHandle);
        //         if(($fieldType == 'Entries' OR $fieldType == 'Users') AND is_string($value)) {
        //             $entry->setRawPostContent($fieldHandle, [$value]);
        //         }
        //     }

        //     // d($entryContent);
        //     // d($entryContent->getAttributes());
        // });
    }
}