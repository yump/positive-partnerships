<?php
namespace Craft;

class YumpForm_FormGeneratorService extends BaseApplicationComponent
{
	private $_entry = null;

    /**
     * All our custom form fields needs to be declared here.
     * array of Strings (name of the macro)
     */
    protected $customMacroList = [
        'submit',
        'chosen',
        'rating',
        'matrix',
        'richtext',
        'fileupload',
        'plaintext',
        'plaintextarea',
        'selectize',
        'yumpdate',
        'yumpcheckbox',
    ];

    /**
     * create a form
     * @param  array  $args [description]
     * @return html template to be used in front end
     */
    public function create($args = []) {
        $originalPath = craft()->path->getTemplatesPath();
        $newPath = craft()->path->getPluginsPath() . 'yumpform/templates';
        craft()->path->setTemplatesPath($newPath);
        $html = craft()->templates->renderMacro('yump_form', 'create', array($args));
        craft()->path->setTemplatesPath($originalPath);
        return TemplateHelper::getRaw($html);
    }

    public function end() {
        $this->_entry = null;
        return TemplateHelper::getRaw('</form>');
    }

    public function input($macro, $args = []) {
        if(!empty($args['horizontal'])) {
            if(empty($args['labelClass'])) {
                $args['labelClass'] = 'col-sm-3 col-md-2';
            }
            if(empty($args['hrInputWrapperClass'])) {
                $args['hrInputWrapperClass'] = 'col-sm-9 col-md-10';
            }
        }
        return $this->_useCraftForm($macro, $args);
    }

    private function _useCraftForm($macro, array $args)
    {
        /** 
         * Add custom class 'yump-form-field' to the form field
         * Please note that: $args['class'] must be a string, otherwise this would returns error.
         * But if it is not string, Craft will return error later on anyway.
         * So, just make sure it is string.
         * For multiple classes, just use space in between: 'class1 class2'
         */
        if(empty($args['class'])) {
            $args['class'] = 'yump-form-field';
        } else {
            $args['class'] .= ' yump-form-field';
        }

        // Get the current template path
        $originalPath = craft()->path->getTemplatesPath();

        // if not custom macro, use the Craft CP form template
        if(!$this->_isCustomMacro($macro)) {
            $newPath = craft()->path->getCpTemplatesPath();
            $fileSubPath = '_includes/forms';
        }
        else {
            $newPath = craft()->path->getPluginsPath() . 'yumpform/templates';
            $fileSubPath = 'yump_form';
        }

        craft()->path->setTemplatesPath($newPath);
        $html = craft()->templates->renderMacro($fileSubPath, $macro, array($args));
        //restore the template path
        craft()->path->setTemplatesPath($originalPath);

        return TemplateHelper::getRaw($html);
    }

    private function _isCustomMacro($macro) {
        return in_array($macro, $this->customMacroList);
    }


    /***************************************************************************/
    /** The following functions are specificly to be used to create entry form / entry form fields */

    /**
     * extend create() function: create an entry form (edit entry).
     * To create a 'add entry form', please use createAddEntryForm()
     * @param  array  $args    
     * @return html template to be used in Twig
     */
    public function createEntryForm($entryId, $args = []) {
        $args['entryId'] = $entryId;
        $args['saveEntry'] = true;
        $entry = craft()->entries->getEntryById($entryId);
        $this->_entry = $entry;
        return $this->create($args);
    }

    public function createAddEntryForm($sectionId, $args = []) {
        $this->_entry = null;
        $args['sectionId'] = $sectionId;
        $args['saveEntry'] = true;
        return $this->create($args);
    }

    public function createUserForm($uId, $args = []) {
        $uId = empty($uId) ? 0 : $uId;
        $args['userId'] = $uId;
        $args['saveUser'] = true;
        return $this->create($args);
    }

    public function field($fieldHandle, $type = 'plaintext', $args = []) {
        // if(empty($this->_entry)) {
        //     return false;
        // }

        // $entry = $this->_entry;
        
        // if(is_object($entry) AND is_string($fieldHandle)) {
        //     $args['errors'] = $entry->getErrors($fieldHandle);

        //     if(empty($args['name'])) {
        //         $args['name'] = 'fields[' . $fieldHandle . ']';
        //     }

        //     if(empty($args['value'])) {
        //         $args['value'] = $entry[$fieldHandle];
        //     }
        // }
        

        if(is_string($fieldHandle)) {
            $fieldHandle = trim($fieldHandle);
            if(strrpos($fieldHandle, '.') === strlen($fieldHandle) - 1) {
                $fieldHandle = trim($fieldHandle, '.');
                $suffix = '[]';
            }
            
            if(empty($args['name'])) {
                $args['name'] = 'fields[' . $fieldHandle . ']' . (empty($suffix) ? '' : $suffix);
            }
            $args['field'] = $fieldHandle;
        }
        
        return $this->input($type, $args);
    }

    /**
     * create the title field for entry form
     * @return [type] [description]
     */
    public function title($title, $args = []) {
        $args['name'] = 'title';
        $args['value'] = $title;
        $args['field'] = 'title';
        return $this->input('plaintext', $args);
    }

    /**
     * relatedEntries: render HTML of a 'entry' or 'user' type field.
     *
     * NOTE: we currently require '$sectionHandle' variable, which is easier, but not ideal, because the field's setting could be changed in CMS, and the related section type might not be the $sectionHandle we defined here, as well as $limit, etc. So, the best way to do it is to list all the settings of this field and load data / generate HTML according to that. It's half way done (in the commented codes after 'return'), but it's gonna take a while to complete it, so we might consider doing it in the future.
     * 
     * @param  [string] $sectionHandle 
     * @param  [UserModel / EntryModel]  $element
     * @param  [string]  $fieldHandle
     * @param  [array] $args  - see 'chosen.html' for references
     * @return html template to be used in Twig
     */
    public function relatedEntries($sectionHandle, $element, $fieldHandle, $args = []) {
        $sectionEntries = craft()->yump->getAllEntriesBySectionHandle($sectionHandle);
        $selectedEntries = $element->getFieldValue($fieldHandle);
        $values = [];
            
        foreach ($selectedEntries as $selectedEntry) {
            $values[] = $selectedEntry->id;
        }
        $options = [];
        foreach ($sectionEntries as $sectionEntry) {
            $options[] = [
                'label' => $sectionEntry->title,
                'value' => $sectionEntry->id,
            ];
        }
        $args['values'] = $values;
        $args['options'] = $options;

        return $this->field($fieldHandle . '.', 'selectize', $args);

        /**
         * Check the comments for this function, indicating what are these codes for.
         */
        // $field = craft()->fields->getFieldByHandle($fieldHandle);
        // if(empty($field->settings) OR empty($field->settings->sources)) {
        //     return $this->_cannotRenderMessage('The field given does not have correct settings.');
        // }
        // $fieldSettings = $field->settings;
        // $limit = $fieldSettings->limit;
        // $sources = $fieldSettings->sources;
        // foreach($sources as $sourceSection) {

        // }
    }

    private function _cannotRenderMessage($message = 'Unknown.') {
        return TemplateHelper::getRaw('<p>We cannot render this field based on your settings. Reasons: ' . $message . '</p>');
    }

    /**
     * Restrictions:
     * 1. Currently only works for matrix containing two fields types: text and textarea.
     * 2. Currently only works for matrix with single block type.
     * 3. Ok, currently it only works for employmentHistory (for vollie), I'm a bit tired to make it more dynamic. (when it comes to writing js to add new block, too tired to find out a way to pull live matrix block settings)
     * 4. Please be careful that the validation of user input and the CMS settings might not match. (we hardcode the settings here)
     * @param  [UserModel / EntryModel] $entry       can be $user or $entry
     * @param  [string] $fieldHandle [description]
     * @param  array  $args        [description]
     * @return [html that can be used in twig template]              [description]
     */
    public function yumpMatrix($entry, $fieldHandle, $args = []) {
        $matrixValues = craft()->yump->getMatrixValueArray($entry, $fieldHandle);
        $args['value'] = $matrixValues;
        $args['name'] = $fieldHandle;

        return $this->input('matrix', $args);
    }

    public function entriesField($fieldHandle, $isHidden = true) {
        
    }

    public function isValidTextInputType($type) {
        return in_array($type, array(
            'text',
            'email',
            'number',
            'password',
            'tel',
            'url',
        ));
    }

    /**
     * this is not working properly atm. It can pull all the html of the matrix field, but the js / css are not rendered properly. Might be useful for the future.
     */
    public function getMatrixFieldHtml($entry, $matrixFieldHandle) {
        $matrix = null;
        $fields = $entry->getFieldLayout()->getFields();
        foreach($fields as $fieldLayoutField) {
            $field = $fieldLayoutField->getField();
            if($field->handle === $matrixFieldHandle) {
                $matrix = $field;
                break;
            }
        }

        if(empty($matrix)) {
            return null;
        }

        $originalPath = craft()->path->getTemplatesPath();
        $newPath = craft()->path->getCpTemplatesPath();
        craft()->path->setTemplatesPath($newPath);
        craft()->templates->includeCssResource('css/craft.css');
        // craft()->templates->includeCssFile('/src/css/craft-matrix.css');
        // craft()->templates->includeCssResource('css/dashboard.css');
        // craft()->templates->includeJs('window.Craft = {}', true);
        // craft()->templates->includeJsResource('lib/garnish-0.1.min.js');
        craft()->templates->includeJsFile('/src/js/custom-matrix-control.js');
        $html = $matrix->getFieldType()->getInputHtmlExcludeJs($matrixFieldHandle, $entry->$matrixFieldHandle);
        craft()->path->setTemplatesPath($originalPath);
        return TemplateHelper::getRaw($html);
    }
}

