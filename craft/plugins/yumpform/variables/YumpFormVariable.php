<?php
namespace Craft;

/**
 * 
 */
class YumpFormVariable
{
    /** 
     * If function in this class not found, go find in in service class
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(craft()->yumpForm_formGenerator, $method), $arguments);
    }   
}