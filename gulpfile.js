var gulp = require('gulp');
var sass = require('gulp-sass');
// var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var filter = require('gulp-filter');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin'); 
var stripDebug = require('gulp-strip-debug');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var path = require('path');
var inject = require('gulp-inject');

//----------------- CONFIGURATION SECTION -------------------------
// Where should gulp look when reading/writing files
var paths = {};
paths.sourceFiles = 'src/';
paths.destinationFiles = 'public/';
paths.styles = [paths.sourceFiles + 'styles/**/*.scss'];
paths.coreScripts = ['node_modules/bootstrap/dist/js/bootstrap.js','node_modules/jquery/dist/jquery.js', 'node_modules/tether/dist/js/tether.js', paths.sourceFiles + 'js/core/**/*.js'];
paths.projectScripts = [paths.sourceFiles + 'js/project/**/*.js'];
paths.images = [paths.sourceFiles + 'img/**/*', '!' + paths.sourceFiles + 'img/svg/', '!' + paths.sourceFiles + 'img/svg/**'];
paths.svg = [paths.sourceFiles + 'img/svg/**/*.svg'];
paths.html = ['craft/templates/**/*.html'];
// paths.fonts = [paths.sourceFiles + 'fonts/*'];
//-----------------------------------------------------------------

// Default dev tasks here (when gulp is run with no parameters)
gulp.task('default', 
  // Run all these tasks:
  ['styles','html','scripts','fonts','images','svg'], 
  // Plus the function below:
  function(){
  browserSync.init({
      proxy: "positive-partnerships.test",
      open: false,
      reloadOnRestart: false,
      injectChanges: true,
  });
  // Watch the various folders / files for change and run respective tasks
  gulp.watch(paths.styles, ['styles']).on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  gulp.watch(paths.html,['html']);
  gulp.watch([paths.coreScripts,paths.projectScripts],['scripts']);
  gulp.watch(paths.images,['images']);
  gulp.watch(paths.fonts,['fonts']);
  gulp.watch(paths.svg,['svg']);
});

// Styles Task: compile SASS, write source-maps
gulp.task('styles', function () {
  // You might like to check to ensure these options match styles-prod task below
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded',
  includePaths: ['node_modules/bootstrap/scss','node_modules/smartmenus/dist/addons/bootstrap-4']
  //includePaths: require('node-normalize-scss').with('node_modules/susy/sass')
};
  return gulp
    .src(paths.styles)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('/maps', { sourceRoot: null }))      
    .pipe(gulp.dest(paths.destinationFiles + 'styles/'))
    // the follwing line lets through only files with a *.css extension - so maps file update doesn't force a page reload
    .pipe(filter(['**/*.css']))  
    .pipe(browserSync.stream());
});

// Compress SVG images
gulp.task('svg', function() {
  return gulp.src(paths.svg)
  .pipe(svgmin(function getOptions (file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
          plugins: [{
              cleanupIDs: {
                  prefix: prefix + '-',
                  minify: true
              }
          }]
      }
  }))
  .pipe(gulp.dest(paths.destinationFiles + 'img/svg'));
});

//Fonts Task
gulp.task('fonts', function() {
  gulp.src(paths.sourceFiles + 'fonts/*')
    .pipe(gulp.dest(paths.destinationFiles + 'css/fonts'));
});

// Just reload the browser when html is updated
gulp.task('html', function () {
    browserSync.reload();
});

//Scripts task: Concatenating, renaming Script files
gulp.task('scripts', function() {
    gulp.src(paths.coreScripts)
      .pipe(plumber())
      // .pipe(concat('main.js'))
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest(paths.destinationFiles + 'js/core'))
    gulp.src(paths.projectScripts)        
       //.pipe(concat('vendor.js'))
       .pipe(rename({suffix: '.min'}))
       .pipe(gulp.dest(paths.destinationFiles + 'js/project'))
      .pipe(browserSync.stream({once: true}));
  });

//Images task: compress and then copy images into public folder
gulp.task('images', function() {
  gulp.src(paths.images)
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest(paths.destinationFiles + '/img'));
});


//Build task
gulp.task('build', ['styles-prod', 'scripts-prod', 'images', 'fonts']);


// Build tasks (no browsersync task, minified scripts and styles, strip debug)
gulp.task('styles-prod', function () {
  // You might like to check to ensure these options match styles task above
var sassBuildOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed',
  includePaths: ['node_modules/bootstrap/scss','node_modules/smartmenus/dist/addons/bootstrap-4']
  //includePaths: require('node-normalize-scss').with('node_modules/susy/sass')
};
  return gulp
    .src(paths.styles)
    .pipe(sourcemaps.init())
    .pipe(sass(sassBuildOptions).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('/maps', { sourceRoot: null }))  
    .pipe(gulp.dest(paths.destinationFiles + 'css/'))
});

gulp.task('scripts-prod', function() {
  gulp.src(paths.coreScripts)
    // .pipe(concat('main.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest(paths.destinationFiles + '/js/core'))
  gulp.src(paths.projectScripts)           
  //   .pipe(concat('vendor.js'))
     .pipe(rename({suffix: '.min'}))
     .pipe(stripDebug())
     .pipe(uglify())
     .pipe(gulp.dest(paths.destinationFiles + '/js/project'))
});
