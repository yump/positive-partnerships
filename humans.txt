                                                                         
  _   _ _   _ _ __ ___  _ __         ___ ___  _ __ ___         __ _ _   _ 
 | | | | | | | '_ ` _ \| '_ \       / __/ _ \| '_ ` _ \       / _` | | | |
 | |_| | |_| | | | | | | |_) |  _  | (_| (_) | | | | | |  _  | (_| | |_| |
  \__, |\__,_|_| |_| |_| .__/  (_)  \___\___/|_| |_| |_| (_)  \__,_|\__,_|
  |___/                |_|                                                

 This site was proudly produced by the team at Yump.  http://yump.com.au
 Follow @YumpDigital on Twitter.


/* TEAM */

	Creative Director: Yuan Wang (@YuanWangTweets)
	Technical Director: Simon East (@SimoEast)
	Project Manager: Brian Truax
	Lead Developer: Wei Lin
	Location: Melbourne, Australia

/* SITE */

	Standards: HTML5, CSS3
	Components: jQuery 1.11, Bootstrap 3, Craft CMS
	Software: Sublime Text 3, Stylizer 5, Photoshop CC 2014

/* THANKS */						
