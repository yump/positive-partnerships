$(function() {
	$('.banner-slides-wrapper').on('init', function(event, slick){
		$(this).css({"visibility": "visible"});

		/*
		Making Slick Sliders Accessible
		*/
		// slick.$slides.each(function () {
			// console.log(this);
			// var $id = $(this).attr("aria-describedby"); 
			// console.log($id); //undefined WHY?
		// });
		//Using the heading as the describedBy target in Banners
		// $('.banner-slide-heading').each(function () {
		    // var $id = $(this).closest('.banner-slide').attr("aria-describedby"); 
		    // console.log($id); //undefined WHY?
		    // ignore extra/cloned slides
		    // if ($slide.attr('aria-describedby') != undefined) { 
		    //     $(this).attr('id', $slide.attr('aria-describedby'));
		    // }
		// });	

	});
	$(window).on('resize', debounce(initOrUpdateBannerSlider, 250)).trigger('resize');

	$('.banner-explore').click (function () { 
		var top = $('#main').offset().top;
		$('html,body').animate({
	        scrollTop: top
	    }, 600);
	});
});

function initOrUpdateBannerSlider() {
	$('.banner-slide').height($('#banner').height());

	var $carousel = $('.banner-slides-wrapper');

	if(!$carousel.length){
		return;
	}

	if($carousel.hasClass('slick-initialized')) {
		$carousel.slick('setPosition');
		return;
	}

	$carousel.slick({
	    infinite: true,
	    slidesToShow: 1,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    arrows: true,
	    dots: $carousel.attr('data-dots') ? true : false,
	    pauseOnDotsHover: true,
	    slidesToScroll: 1,
	    prevArrow: '<a type="button" class="slick-prev slick-arrow"><img src="/src/img/core/arrow-left.png" alt="Previous Slide"></a>',
	    nextArrow: '<a type="button" class="slick-next slick-arrow"><img src="/src/img/core/arrow-right.png" alt="Next Slide"></a>',
	});
}