jQuery(document).ready(function($){

    $('.image-slider-wrap').each(function() {
		var self = this;
		$(self).on('init', function(event, slick){
			resetImageSlick(self, slick);
		});
	});

    if($('.image-slider-wrap').length){
    $('.image-slider-wrap').slick({
        // dots: true,
        adaptiveHeight: true,
        autoplay: true,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        fade: true,
        autoplaySpeed: 5000,
        prevArrow: '<div class="slider-arrow arrow-previous"><img src="/src/img/core/slide-previous.png" alt="previous slide"></div>',
        nextArrow: '<div class="slider-arrow arrow-next"><img src="/src/img/core/slide-next.png" alt="next slide"></div>',
    });
    }
    $(window).on('resize', debounce(function() {
    	$('.image-slider-wrap').each(function() {
    		if($(this).hasClass('slick-initialized')) {
    			resetImageSlick(this, $(this).slick('getSlick'));
    		}
    	});
	}, 250));

    // turn off autoplay to avoid page jumping after scrolling passes a certain point.
    $(window).on('scroll', function() {
        $('.image-slider-wrap').each(function() {
            if($(this).hasClass('slick-initialized')) {
                var slick = $(this).slick('getSlick');
                if($(window).scrollTop() > $(this).offset().top + $(this).find('.slick-list').height() / 2) {
                    if(slick.slickGetOption('autoplay')) {
                        slick.slickSetOption('autoplay', false, true);
                    }
                } else {
                    if(!slick.slickGetOption('autoplay')) {
                        slick.slickSetOption('autoplay', true, true);
                    }
                }
            }
        });
    }).trigger('scroll');
});

function resetImageSlick(target, slick) {
	slick.setPosition();
	$(target).css({'visibility': 'visible', 'position': 'relative'});
}
