$(function() {
	/* We use the old library of 'Waypoint' plugin, because the 3.0+ is buggy with this counterUp plugin when keep scrolling up and down. */
	$('.stats-counter-number__number').counterUp({
		delay: 10,
    	time: 1000
	});
});
