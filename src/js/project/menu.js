// https://bootbites.com/articles/freebie-sticky-bootstrap-navbar-scroll-bootstrap-4/

$(document).ready(function() {
  // Custom
  var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
    var stickyHeight = sticky.outerHeight();
    var stickyTop = stickyWrapper.offset().top;
    if (scrollElement.scrollTop() >= stickyTop){
      stickyWrapper.height(stickyHeight);
      //our header element is actually bigger (as its a two tier header) so hardcoding the height here instead
      // stickyWrapper.height(80);
      sticky.addClass("is-sticky");
      // sticky.removeClass("at-top");
    }
    else{
      sticky.removeClass("is-sticky");
      stickyWrapper.height('auto');
      // if (scrollElement.scrollTop() == 0){
      //   sticky.addClass("at-top");
      // }
    }
  };

  // Find all data-toggle="sticky-onscroll" elements
  $('[data-toggle="sticky-onscroll"]').each(function() {
    var sticky = $(this);
    var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
    sticky.before(stickyWrapper);
    sticky.addClass('sticky');

    // Scroll & resize events
    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
      stickyToggle(sticky, stickyWrapper, $(this));
    });

    // On page load
    stickyToggle(sticky, stickyWrapper, $(window));
  });

  $('.navbar-toggler').on('click', function () {
    if ($(this).hasClass('collapsed')) {
      $('#main-content, footer').addClass('darken');
    } else {
      $('#main-content, footer').removeClass('darken');
    }
  })

  // Function for creating sticky side nav on desktop
  function stickyNav() {
    if ($('.sidebar').length === 1) {
      var $sticky = $('.sidebar__wrap');
      var sticky = $('.sidebar__wrap')[0];
      var stickyHeight = sticky.scrollHeight;
      var navHeight = $('.top-strip')[0].scrollHeight;
      var stickManualOffset = -20;
      var stickOffset;
      var stickyStop;
      var stickyStopClass;
      var windowTop;
      var stickyStart;
      $(window).scroll(function () {
        if ($(window).width() > 991 && $(window).width() < 1200) {
          stickManualOffset = -36;
        } else if ($(window).width() > 1200) {
          stickManualOffset = -70;
        }
        stickOffset = navHeight + stickManualOffset;
        windowTop = $(window).scrollTop();
        stickyStart = $('.sidebar')[0].offsetTop - stickOffset;
        if ($('.body-neo-wrapper--full-width').length > 0) {
          stickyStopClass = '.body-neo-wrapper--full-width'
        } else {
          stickyStopClass = 'footer'
        }
        stickyStop = $(stickyStopClass)[0].offsetTop - stickOffset - 30;
        if (windowTop > stickyStart && windowTop < stickyStop - stickyHeight) {
          $sticky.css({position: 'fixed', top: stickOffset});
        } else if (windowTop <= stickyStart) {
          $sticky.css({ position: 'static' });
        } else {
          $sticky.css({position: 'absolute', top: $(stickyStopClass)[0].offsetTop - stickyHeight - 30});
        }
      });
    }
  }
  stickyNav();

});
