var initShowingItems = 6;
var increment = 6;

// make 'currentShowing' always (initShowingItems + increment * x), to avoid showing weird number of items with load more button
function validateCurrentShowing(rawNumber) {
  // my weird math stuff...
  var m = (rawNumber - initShowingItems) / increment;
  var x = isInt(m) ? m : Math.floor(m + 1);
  var validatedVal = increment * x + initShowingItems;
  // console.log(validatedVal);
  return validatedVal;
}

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

$('#resourcesFilter li button').click(function(){
	console.log('working');
	$('.resources-filter__filters__mobile-active').removeClass('show');
});

window.resourcesFilterVue = new Vue({
	el: "#resources-filter-vue",
	delimiters: ['{*', '*}'],
	data: {
		rawResourceEntries: null,
		currentShowing: urlShowingParam && parseInt(urlShowingParam) ? validateCurrentShowing(parseInt(urlShowingParam)) : initShowingItems,
		resourceCategories: null,
		activeCategory: 'All Resources',
		activeCategoryName: 'All Resources',
		ajaxFail: false
	},
	computed: {
		filterdResources: function() {
			var self = this;
			if(this.rawResourceEntries) {
				var filterdResources = this.rawResourceEntries.filter( function(resourceEntry) {
					if (self.activeCategory == 'All Resources') {
						return true;
					}
					if(resourceEntry.categories.length) {
						var i;
						for (i = 0; i < resourceEntry.categories.length; i++) {
							if (resourceEntry.categories[i].id == self.activeCategory) {
								return true;
							}
						}
					} else {
						return false;
					}
				});
			}
			return filterdResources;
		},
		cappedFilterdResources: function() {
	        if(this.filterdResources) {
	          return this.filterdResources.slice(0, this.currentShowing); // if currentShowing is larger than the length, will return the full filterdResources array instead of an error
	        }
	        return null;
	    },
		showLoadMore: function() {
			if(this.filterdResources) {
				if(this.currentShowing < this.filterdResources.length) {
					return true;
				}
			}
			return false;
		}
	},
	methods: {
		loadMore: function(currentShowing) {
			if((this.currentShowing + increment) < this.filterdResources.length) {
				this.currentShowing = this.currentShowing + increment;
			} else {
				this.currentShowing = validateCurrentShowing(this.filterdResources.length);
			}
		},
		filterResources: function(category) {
			this.activeCategory = category;
			if(this.resourceCategories[category]) {
				this.activeCategoryName = this.resourceCategories[category];
			} else {
				this.activeCategoryName = 'All Resources';
			}

		}
	},
	watch: {
		currentShowing: function(newVal) {
			updateUrlParams('showing', newVal);
		},
	}
});

$(function() {
	$.ajax({
		dataType: "json",
		method: "GET",
		url: "/actions/positivePartnerships/getNotFeaturedResources",
		data: {
			parentID: entryID
		},
		timeout: 30000, //default: 30 seconds
	})
	.done(function(response, textStatus, jqXHR) {
    	// console.log(response.resources);
		if(response.success) {
			window.resourcesFilterVue.rawResourceEntries = response.resources.resources;
			window.resourcesFilterVue.resourceCategories = response.resources.allCategories;
		} else {
      window.resourcesFilterVue.ajaxFail = true;
    }
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
    // console.log('fail');
		window.resourcesFilterVue.ajaxFail = true;
	});

});
