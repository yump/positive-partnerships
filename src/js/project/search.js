/** 
* Filtering
**/

function updateQuery(checkbox) {
  //console.log(checkboxes);
  var uri = location.href;
  var key = checkbox.val();
  // console.log(uri);
  // $(checkboxes).each( function() { });
  if (checkbox.prop('checked')) { 
    var value = 1;
    var updatedURI = updateQueryStringParameter(uri, key, value);
  } else {
    var value = 0;
    var updatedURI = updateQueryStringParameter(uri, key, value);
  }
  redirectPage(updatedURI);

}

function updateFrontend(){
  $('.search-results-list').addClass('filtering');
  $(checkboxes).attr('disabled', true);
}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

function redirectPage(uri){
  document.location.replace(uri);
}

var checkboxes = $('.refine-search-controls .form-check-input');
$(checkboxes).change(function (i) {
  updateFrontend($(this));
  updateQuery($(this));
});