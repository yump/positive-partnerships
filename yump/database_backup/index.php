<?php
// DATABASE BACKUP SCRIPT
// By Simon East for Yump, original version from August 2012
// Updated for Craft CMS Nov 2016
//
// This script needs to be called from one of the IP addresses below, using the password, eg.
// http://xxxxxxxx.com/yump/database_backup/?a=z8tmq0274jls
//------------------------- Configuration Section --------------------------------------------
$PASSWORD = '';
$ALLOWED_IPs = array(
    '127.0.0.1', 
    '203.191.201.182',        // Epic office
    '43.241.54.230',          // staging.yump.com.au (new server)
);

$pathToDatabaseConfig = '../../craft/config/db.php';
//--------------------------------------------------------------------------------------------



	// For reference, the OLD method of dumping databases was:
	// (Note: this requires a CPanel setting to allow remote MySQL access for our IP address)
	// mysqldump --user=... --password=... your_database_name | gzip > "your_database_name-$(date +%Y-%m-%d-%H.%M.%S).sql.gz"
	// $a = `mysqldump -h m4-mysql1-1.ilisys.com.au -u surfcpp -p5YFJBdI8 surfcpp_db`;
	// $a = `mysqldump -h 10.1.1.121 -u root -psurface -d sbmssurf_surfcms`;
	// $a = `mysqldump -h localhost -u sbmssurf_n9deKXS -pq1EevF63H2OuZj -d sbmssurf_surfcms`;


	
error_reporting(E_ALL);
ini_set('display_errors', 1);

// For security we're going to require:
//	 - password in querystring
//	 - correct IP
//	 - AND we could also implement a salted hash that changes every hour
// Without these, we don't give access to our precious data
if ($_GET['a'] == $PASSWORD 
	&& in_array($_SERVER['REMOTE_ADDR'], $ALLOWED_IPs)
	// && $_GET['b'] == md5(date('ymdH') . $PASSWORD . 'surface')
	) {

	require('mysqldump.class.php');
	$my = new MySQLDump();
	
	// Get the database connection details from Craft's config file
	$dbConfig = require($pathToDatabaseConfig);
	
	// Normalize it to a multi-environment config
	if (!array_key_exists('*', $dbConfig))	{
		$dbConfig = array('*' => $dbConfig);
	}

	// Loop through all of the environment configs, figuring out what the final word is on Dev Mode
	foreach ($dbConfig as $env => $envConfig) {
		if ($env != '*' && strpos($_SERVER['HTTP_HOST'], $env) !== false) {
			$dbConfig['*'] = array_merge($dbConfig['*'], $envConfig);
		}
	}
	
	$ok = $my->connect(
		$dbConfig['*']['server'],
		$dbConfig['*']['user'],
		$dbConfig['*']['password'],
		$dbConfig['*']['database']
	);
	
	if ($ok === true)
		$my->dumpAll();
	else
		header("HTTP/1.1 500 Database connection failed: $ok");

	
} else { 
	// If the password or IP is incorrect
	header('HTTP/1.1 401 Unauthorized');
	echo 'Oops, authentication failed. Your IP address ' . $_SERVER['REMOTE_ADDR'] . ' is not allowed.';
}
