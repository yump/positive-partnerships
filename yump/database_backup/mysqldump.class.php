<?php
/**
 * Database MySQLDump Class File
 * Based upon James Elliott's PHP class
 * James.d.Elliott@gmail.com
 * GNU General Public License v3 http://www.gnu.org/licenses/gpl.html
 * 
 * Significantly rewritten by Simon East for Surface Digital, August 2012
 * simon@surfacedigital.com.au
 *
 * Modified by Wei in 2016 for mysqli:
 * - mysql library is deprecated, so we have to change into mysqli and tweak the codes because of that.
 */

$version1 = '2.0.0'; // This Scripts Version.

class MySQLDump {

	var $connected = false;
	var $droptableifexists = true;
	var $asAttachment = 1;
	var $tablesAndViews = array();		// cache the list here
	
	var $dbConn = null;
	
	function connect($host, $user, $pass, $db) {
		$this->dbConn = mysqli_connect($host, $user, $pass);
		if (!$this->dbConn)
			return mysqli_error();
		mysqli_select_db($this->dbConn, $db);
		$this->connected = true;
		
		if ($this->asAttachment) {
			// We'll be outputting an SQL attachment
			header('Content-type: application/octet-stream');
			header('Content-Disposition: attachment; filename="' . $_SERVER['HTTP_HOST'] . '-' . $db . '.sql"');
		}
		header('X-Robots-Tag: noindex, nofollow');	// tell robots to ignore this completely, we do NOT want them indexing our stuff
		ini_set('max_execution_time', 30 * 60);		// 30mins
		return true;
	}
	
	function header() {
		echo "SET FOREIGN_KEY_CHECKS=0;
/*!40101 SET NAMES utf8 */;
";		
	}
	
	function footer() {
		echo "\nSET FOREIGN_KEY_CHECKS=1;";
	}

	function listTablesAndViews() {
		if (!$this->connected) return array();
		$list = array();
		$sql = mysqli_query($this->dbConn, "SHOW FULL TABLES");
		while ($row = mysqli_fetch_row($sql))
			$list[] = $row;
		$this->tablesAndViews = $list;
		return $list;
	}
	
	// Returns an array with the name of all the tables in the current DB (excludes views)
	function listTables() {
		if (!$this->tablesAndViews) $this->listTablesAndViews();
		$list = array();
		foreach ($this->tablesAndViews as $i)
			if ($i[1] == 'BASE TABLE')
				$list[] = $i[0];
		return $list;
	}
	
	// Returns an array with the name of all views in the current DB
	function listViews() {
		if (!$this->tablesAndViews) $this->listTablesAndViews();
		$list = array();
		foreach ($this->tablesAndViews as $i)
			if ($i[1] == 'VIEW')
				$list[] = $i[0];
		return $list;
	}
	
	// Dumps all tables & views in the selected database except those
	// provided in the array
	function dumpAll($excludeTables = array()) {
		$this->header();
		foreach ($this->listTables() as $table)
			if (!in_array($table, $excludeTables))
				$this->dumpTable($table);
		foreach ($this->listViews() as $view)
			if (!in_array($view, $excludeTables))
				$this->dumpTableStructure($view);
		$this->footer();
	}

	
	// Dumps both structure and values
	// (not recommended for use on a view)
	function dumpTable($tablename) {
		$type = $this->dumpTableStructure($tablename);	
		$this->dumpTableValues($tablename);
	}

	// Echoes CREATE TABLE (or CREATE VIEW) statement
	function dumpTableStructure($tablename) {
		echo "\n-- Dumping structure for table: $tablename\n\n";
		if ($this->droptableifexists) {
			if (in_array($tablename, $this->listViews()))
				echo "DROP VIEW IF EXISTS `$tablename`;\n";
			else
				echo "DROP TABLE IF EXISTS `$tablename`;\n";
		}
		$sql = mysqli_query($this->dbConn, "SHOW CREATE TABLE `$tablename`");
		$sql = mysqli_fetch_row($sql);
		$sql = $sql[1];
		// Remove annoying ALGORITHM/DEFINER stuff that doesn't migrate well across servers
		$sql = preg_replace('/ALGORITHM.*?DEFINER /', '', $sql);
		echo $sql, ";\n";
	}

	// Dumps all the values for a table/view
	function dumpTableValues($tablename) {
		$sql = mysqli_query($this->dbConn, "SELECT * FROM `$tablename`");
		echo "\n-- Dumping data for table: $tablename\n\n";
		
		// Foreach row
		$i = 0;
		while ($row = mysqli_fetch_assoc($sql)) {
			if ($i++ % 100 == 0) {
				if ($i > 1) echo ";\n";						// 100th row
				echo "INSERT INTO `$tablename` VALUES"; 	// 1st, 101st, etc rows...
			} else
				echo ",";
			$vals = array();
			foreach ($row as $key => $val) {
				if (is_null($val))								// handle NULLs
					$vals[] = 'NULL';
				elseif (is_numeric($val) && !is_string($val))	// handle numbers (but also watch for postcodes and phone numbers beginning with zero)
					$vals[] = $val;
				else											// assume anything else is a string
					$vals[] = "'" . mysqli_real_escape_string($this->dbConn, $val) . "'";
			}
			echo "\n(" . implode(',', $vals) . ")";
			flush();
		}
		echo ";\n";
		flush();
	}



}
