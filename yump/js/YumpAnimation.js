$(document).ready(function() {
	/**
	 * Simon's scroll magic scripts 
	 * 
	 * require library: //cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js
	 * or get the latest from their website http://scrollmagic.io/
	 */
	var controller = new ScrollMagic.Controller();
        
    // Here we set up some classes that we can reuse across the site to do different
    // types of transitions when the window is scrolled and the object becomes visible
        
    $('.animate-when-visible.animate-from-left')
        // Set initial state, prior to animation
        .css({
            transform: 'translateX(-80px)',
            // rotate: '-30deg',
            opacity: 0
        });
        
    $('.animate-when-visible.animate-from-bottom')
        // Set initial state, prior to animation
        .css({
            transform: 'translateY(40px)',
            // rotate: '-30deg',
            opacity: 0
        });

    $('.animate-when-visible.animate-from-top')
        // Set initial state, prior to animation
        .css({
            transform: 'translateY(-40px)',
            // rotate: '-30deg',
            opacity: 0
        });
        
    $('.animate-when-visible.animate-from-right')
        // Set initial state, prior to animation
        .css({
            transform: 'translateX(80px)',
            // rotate: '30deg',
            opacity: 0
        });
        
    $('.animate-when-visible.animate-rotate-from-left')
        // Set initial state, prior to animation
        .css({
            transform: 'translateX(-150%)',
            rotate: '-30deg',
            opacity: 0.3
        });
        
    $('.animate-when-visible.animate-rotate-from-right')
        // Set initial state, prior to animation
        .css({
            transform: 'translateX(150%)',
            rotate: '30deg',
            opacity: 0.3
        });
        
    $('.animate-when-visible.animate-zoom-in')
        // Set initial state, prior to animation
        .css({
            // transform: 'translateX(150%)',
            transform: 'scale(0.9)',
            rotate: '5deg',
            opacity: 0
        });
        
    // Here we use ScrollMagic to trigger the animation when the object
    // comes into the viewport
    // It triggers a TweenLite animation, provided by the GSAP library
    //
    // See:
    // http://scrollmagic.io/
    // https://greensock.com/gsap
        
    $('.animate-when-visible')
        .each(function(){
            new ScrollMagic.Scene({triggerElement: this, triggerHook: 0.95})
            .setTween(this, 0.5, {
                x: 0, 
                y: 0,
                scale: 1,
                rotate: 0,
                opacity: 1,
                delay: $(this).data('animation-delay')
            })
            // .addIndicators({name: "1 (duration: 0)"}) // add indicators (requires plugin)
            .addTo(controller);
        });
    /** End of Simon's scroll magic scripts */
});