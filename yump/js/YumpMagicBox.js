$(document).ready(function() {
	/********** Superaaa Yump Magic Hover! *******/
	/*
	 * This will tell all the siblings to fade to half opacity when hover
	 *
	 * Require: JQuery
	 *
	 * Usage Example: 
	   <ul>
	       <li class="yump-magic-hover">Menu 1</li>
	       <li class="yump-magic-hover">Menu 2</li>
	       <li class="yump-magic-hover">Menu 3</li>
	   </ul>

	   Usage Example 2: (for those items with <a> wrappers)
	   <ul>
	   		<a class="yump-magic-hover-parent" href="#">
	   			<li class="yump-magic-hover">Menu 1</li>
	   		</a>
	   		<a href="yump-magic-hover-parent">
	   			<li class="yump-magic-hover">Menu 1</li>
	   		</a>
	   		<a href="yump-magic-hover-parent">
	   			<li class="yump-magic-hover">Menu 1</li>
	   		</a>
	   </ul>
	 */
	$('.yump-magic-hover').hover(function() {
		$(this).siblings('.yump-magic-hover').css({
			'opacity': '0.5', 
			'-webkit-transition': 'opacity 1s', 
			'-moz-transition': 'opacity 1s', 
			'transition': 'opacity 1s'
		});
		$(this).closest('.yump-magic-hover-parent')
			.siblings('.yump-magic-hover-parent')
			.find('.yump-magic-hover').css({
				'opacity': '0.5', 
				'-webkit-transition': 'opacity 1s', 
				'-moz-transition': 'opacity 1s', 
				'transition': 'opacity 1s'
		});
	}, function() {
		$(this).siblings('.yump-magic-hover').css({
			'opacity': '1', 
			'-webkit-transition': 'opacity 1s',
			'-moz-transition': 'opacity 1s',
			'transition': 'opacity 1s'
		});
		$(this).closest('.yump-magic-hover-parent')
			.siblings('.yump-magic-hover-parent')
			.find('.yump-magic-hover').css({
				'opacity': '1', 
				'-webkit-transition': 'opacity 1s',
				'-moz-transition': 'opacity 1s',
				'transition': 'opacity 1s'
		});
	});


	/********** animateScrollTo ***********/
	/*
	Example: 
	<div class="animateScrollTo" 
	data-scrolltarget="#scrolled-to-section" 
	data-scrollspeed="500" 
	style="cursor: pointer">Explore</div>
	 */
	$('.animateScrollTo').click(function() {
		var target = $(this).data('scrolltarget');
		var speed = $(this).data('scrollspeed');
		if(!speed) {
			speed = 600;
		}
		if(target) {
			$('html,body').animate({
		        scrollTop: $(target).offset().top
		    }, speed);
		}
	});

	/**
	 * prevent double form submission, tweak Simon's codes
	 * To use it, remember to put:
	 * preventdoublesubmit data-yumpvalidate="true"
	 * into your form tag
	 *
	 * data-yumpvalidate="true" will make the form listening to 'validationSuccess' event,
	 * which can be triggered in custom functions: $(form).trigger('validationSuccess');
	 * 
	 */
	$('form[preventdoublesubmit], form[submitonce]').each(function(){
        var $form = $(this);

        var eventToHandle = $form.data('yumpvalidate')
                ? 'validationSuccess'
                : 'submit';

        // console.log($form);
        // console.log($form.data('yumpvalidate'));
        // console.log(eventToHandle);
        // console.log($form.find(':submit'));
        
        // When form is submitted
        bindPreventDoubleFormSummissionEvent($form, eventToHandle);
    });
	
});

function bindPreventDoubleFormSummissionEvent($form, eventToHandle) {
	$form.on(eventToHandle, function(e){
        // Prevent submission if it has already occurred (submit may trigger again for keyboard <ENTER> for example)
        if ($form.data('hasBeenSubmitted')) {
            e.preventDefault();
        }
        $form.data('hasBeenSubmitted', true);
        
        // Disable submit button(s)
        $('form').find(':submit').prop('disabled', 'disabled').css('opacity', 0.6);
    });
}

/**
 * https://davidwalsh.name/javascript-debounce-function
 * js deboucing by David Walsh
 *
 * How to use:
 * $(window).on('resize', debounce(function() {
 * 		// your codes here
 * }, 250));
 */
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};